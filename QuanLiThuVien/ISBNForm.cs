﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLiThuVien
{
    public partial class ISBNForm : Form
    {
        public ISBNForm()
        {
            InitializeComponent();
        }


         private void myForm_Load(object sender, EventArgs e)
        {
            getDataList();
        }
        
        // TODO: change
         private const String SPSelect = "ISBN_XEM";
         private const String SPInsert = "ISBN_THEM";
         private const String SPUpdate = "ISBN_SUA";
         private const String SPDelete = "ISBN_XOA";
        private const Boolean isAutoIncreasePrimaryKey = false;
      

        private BindingSource ngonNguBindingSource;
        private BindingSource theLoaiBindingSource; 
        // ---------------------------------
        private void getDataList()
        {
            myDataGridView = GlobalFunction.getDataList(myDataGridView, SPSelect);



            // get data for maTheLoai ComboBox

            theLoaiBindingSource = Program.getBindingSource("exec THELOAI_XEM");
            for (int i = 0; i < theLoaiBindingSource.List.Count; i++)
            {
                DataRowView current = (DataRowView)theLoaiBindingSource.List[i];
                maTheLoaiComboBox.Items.Add(current["MATL"].ToString());
            }

            // get data for maNgonNgu ComboBox
            ngonNguBindingSource = Program.getBindingSource("exec NGONNGU_XEM");

            for (int i = 0; i < ngonNguBindingSource.List.Count; i++)
            {
                DataRowView current = (DataRowView)ngonNguBindingSource.List[i];
                maNgonNguComboBox.Items.Add(current["MANGONNGU"].ToString());
            }
          
           
            
        }




        private String action = "";
        private void myAddButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.AddNew();
            action = "Add";
            enableActionForAddAndEditButton();
        }
        private void myEditButton_Click(object sender, EventArgs e)
        {
            action = "Edit";
            if (!isAutoIncreasePrimaryKey)
            {
                GlobalFunction.oldPrimaryKey = ISBNTextBox.Text; 
            }
          
            enableActionForAddAndEditButton();
        }

        private void myDeleteButton_Click(object sender, EventArgs e)
        {
            // TODO: change
            String primaryKey = ISBNTextBox.Text;
            GlobalFunction.deleteRow(SPDelete, primaryKey, "Mã Độc giả");
            // -----------------------------
            getDataList();

        }
        private void myDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in myDataGridView.SelectedRows)
            {
                // TODO: change
                try
                {
                    ISBNTextBox.Text = row.Cells[0].Value.ToString();
                    tenSachTextBox.Text = row.Cells[1].Value.ToString();
                    khoSachTextBox.Text = row.Cells[2].Value.ToString();
                    noiDungTextBox.Text = row.Cells[3].Value.ToString();
                    hinhAnhPathTextBox.Text = row.Cells[4].Value.ToString();         
                    if (row.Cells[5].Value.ToString() != String.Empty)
                    {
                        ngayXuatBanTimePicker.Value = Convert.ToDateTime(row.Cells[5].Value.ToString());
                    }
                    
                    lanXuatBanTextBox.Text = row.Cells[6].Value.ToString();
                    soTrangTextBox.Text = row.Cells[7].Value.ToString();
                    giaTextBox.Text = row.Cells[8].Value.ToString();
                     maNgonNguComboBox.SelectedItem = row.Cells[9].Value.ToString();
                     maTheLoaiComboBox.SelectedItem = row.Cells[10].Value.ToString(); 
                 
                  
                  
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message); 
                }
            



                //------------------------------------------------------------

            }


        }

        private void mySaveButton_Click(object sender, EventArgs e)
        {
            // TODO: change ----------------------------------------------

            // ràng buộc 
            if( DateTime.Compare(ngayXuatBanTimePicker.Value , DateTime.Today) > 0 ) { 
                MessageBox.Show("Ngày xuất bản phải bé hơn " + DateTime.Today , "Lỗi" , MessageBoxButtons.OK) ; 
                return ; 
            }

           
            ///////////////////////

            String primaryKey = ISBNTextBox.Text;
            String tenSach = tenSachTextBox.Text;
            String khoSach = khoSachTextBox.Text;
            String noiDung = noiDungTextBox.Text;
            String hinhAnhPath = hinhAnhPathTextBox.Text; 
            String ngayXuatBan = ngayXuatBanTimePicker.Value.ToString("yyyy-MM-dd");
            String lanXuatBan = lanXuatBanTextBox.Text; 
            String soTrang = soTrangTextBox.Text;
            String gia = giaTextBox.Text;
            String maNgonNgu = maNgonNguComboBox.SelectedItem.ToString();
            String maTheLoai = maTheLoaiComboBox.SelectedItem.ToString();
          

            List<String> textBoxValues = new List<String>();
            textBoxValues.Add(primaryKey);
            textBoxValues.Add(tenSach);
            textBoxValues.Add(khoSach);
            textBoxValues.Add(noiDung);
            textBoxValues.Add(hinhAnhPath);  
            textBoxValues.Add(ngayXuatBan);
            textBoxValues.Add(lanXuatBan); 
            textBoxValues.Add(soTrang);
            textBoxValues.Add(gia);
            textBoxValues.Add(maNgonNgu);
            textBoxValues.Add(maTheLoai);
        
            List<String> lableForTextBoxValues = new List<String>();
            lableForTextBoxValues.Add("ISBN");
            lableForTextBoxValues.Add("Tên sách");
            lableForTextBoxValues.Add("Khổ sách");
            lableForTextBoxValues.Add("Nội dung");
            lableForTextBoxValues.Add("Hình ảnh path");
            lableForTextBoxValues.Add("Ngày xuất bản");
            lableForTextBoxValues.Add("Lần xuất bản");
            lableForTextBoxValues.Add("Số trang");
            lableForTextBoxValues.Add("Giá");
            lableForTextBoxValues.Add("Mã ngôn ngữ");
            lableForTextBoxValues.Add("Mã thể loại");
          

            List<int> primaryKeyIndex = new List<int>();
            primaryKeyIndex.Add(0);

        
            myDataGridView = GlobalFunction.saveActionAuto(myDataGridView, isAutoIncreasePrimaryKey, primaryKeyIndex, textBoxValues, lableForTextBoxValues, action, SPInsert, SPUpdate, SPSelect);
            enableActionForSaveAndCancelButton();
         
        }

        private void myCancelButton_Click(object sender, EventArgs e)
        {
            GlobalFunction.cancelAciton();
         

            enableActionForSaveAndCancelButton();
        }

        private void mySearchButton_Click(object sender, EventArgs e)
        {
            GlobalFunction.searchData(mySearchTextBox.Text, myDataGridView);
        }

        private void moveFirstButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveFirst();
        }

        private void MoveLastButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveLast();
        }

        private void movePreviousButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MovePrevious();
        }

        private void moveNextButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveNext();
        }


        private void enableActionForSaveAndCancelButton()
        {
            // TODO: change
            // diable textbox
            ISBNTextBox.Enabled = false;
            tenSachTextBox.Enabled = false;
            khoSachTextBox.Enabled = false;
            noiDungTextBox.Enabled = false;
            hinhAnhPathTextBox.Enabled = false;        
            ngayXuatBanTimePicker.Enabled = false;
            lanXuatBanTextBox.Enabled = false;
            soTrangTextBox.Enabled = false;
            giaTextBox.Enabled = false;
            maNgonNguComboBox.Enabled = false;
            maTheLoaiComboBox.Enabled = false;
           
            ///////////////////////////////////////
            // config button
            myCancelButton.Enabled = false;
            mySaveButton.Enabled = false;
            myAddButton.Enabled = true;
            myEditButton.Enabled = true;
            myDeleteButton.Enabled = true;
            myDataGridView.Enabled = true;
        }
        private void enableActionForAddAndEditButton()
        {
            // TODO: change     
            //config textbox
            if (! isAutoIncreasePrimaryKey)
            {
                ISBNTextBox.Enabled = true;
            } 
            else
            {
                ISBNTextBox.Enabled = false;
            }

            tenSachTextBox.Enabled = true;
            khoSachTextBox.Enabled = true;
            noiDungTextBox.Enabled = true;
            hinhAnhPathTextBox.Enabled = true;
            ngayXuatBanTimePicker.Enabled = true;
            lanXuatBanTextBox.Enabled = true;
            soTrangTextBox.Enabled = true;
            giaTextBox.Enabled = true;
            maNgonNguComboBox.Enabled = true;
            maTheLoaiComboBox.Enabled = true;
            //////////////////////////////////////////
            // config button
            mySaveButton.Enabled = true;
            myCancelButton.Enabled = true;
            myDataGridView.Enabled = false;
            myDeleteButton.Enabled = false;
            myAddButton.Enabled = false;
            myEditButton.Enabled = false;
            ISBNTextBox.Focus();

        }

        private void maNgonNguComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView) ngonNguBindingSource.List[maNgonNguComboBox.SelectedIndex] ;
            tenNgonNguTextBox.Text = currentRow["NGONNGU"].ToString();
        }

        private void maTheLoaiComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView) theLoaiBindingSource.List[maTheLoaiComboBox.SelectedIndex];
            tenTheLoaiTextBox.Text = currentRow["THELOAI"].ToString();
        }
    
    }
}
