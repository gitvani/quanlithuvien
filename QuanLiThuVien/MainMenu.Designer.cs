﻿namespace QuanLiThuVien
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.CapNhatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iSBNToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.theLoaiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ngonNguToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sáchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sachToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sachDienTuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nganTuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tacGiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.táchGiảSáchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.độcGiảToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nhanVienToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.phiếuMượnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chiTiếtPhiếuMượnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quanLiTaiKhoanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mượnSáchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelMain = new System.Windows.Forms.Panel();
            this.trảSáchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CapNhatToolStripMenuItem,
            this.quanLiTaiKhoanToolStripMenuItem,
            this.mượnSáchToolStripMenuItem,
            this.trảSáchToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(879, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // CapNhatToolStripMenuItem
            // 
            this.CapNhatToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iSBNToolStripMenuItem1,
            this.theLoaiToolStripMenuItem,
            this.ngonNguToolStripMenuItem,
            this.sáchToolStripMenuItem,
            this.nganTuToolStripMenuItem,
            this.tacGiaToolStripMenuItem,
            this.táchGiảSáchToolStripMenuItem,
            this.độcGiảToolStripMenuItem,
            this.nhanVienToolStripMenuItem,
            this.phiếuMượnToolStripMenuItem,
            this.chiTiếtPhiếuMượnToolStripMenuItem});
            this.CapNhatToolStripMenuItem.Name = "CapNhatToolStripMenuItem";
            this.CapNhatToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.CapNhatToolStripMenuItem.Text = "Cập nhật";
            // 
            // iSBNToolStripMenuItem1
            // 
            this.iSBNToolStripMenuItem1.Name = "iSBNToolStripMenuItem1";
            this.iSBNToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.iSBNToolStripMenuItem1.Text = "ISBN";
            this.iSBNToolStripMenuItem1.Click += new System.EventHandler(this.iSBNToolStripMenuItem1_Click);
            // 
            // theLoaiToolStripMenuItem
            // 
            this.theLoaiToolStripMenuItem.Name = "theLoaiToolStripMenuItem";
            this.theLoaiToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.theLoaiToolStripMenuItem.Text = "Thể loại";
            this.theLoaiToolStripMenuItem.Click += new System.EventHandler(this.theLoaiToolStripMenuItem_Click);
            // 
            // ngonNguToolStripMenuItem
            // 
            this.ngonNguToolStripMenuItem.Name = "ngonNguToolStripMenuItem";
            this.ngonNguToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ngonNguToolStripMenuItem.Text = "Ngôn ngữ";
            this.ngonNguToolStripMenuItem.Click += new System.EventHandler(this.ngonNguToolStripMenuItem_Click);
            // 
            // sáchToolStripMenuItem
            // 
            this.sáchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sachToolStripMenuItem,
            this.sachDienTuToolStripMenuItem});
            this.sáchToolStripMenuItem.Name = "sáchToolStripMenuItem";
            this.sáchToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.sáchToolStripMenuItem.Text = "Sách";
            // 
            // sachToolStripMenuItem
            // 
            this.sachToolStripMenuItem.Name = "sachToolStripMenuItem";
            this.sachToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.sachToolStripMenuItem.Text = "Sách giấy";
            this.sachToolStripMenuItem.Click += new System.EventHandler(this.sachToolStripMenuItem_Click);
            // 
            // sachDienTuToolStripMenuItem
            // 
            this.sachDienTuToolStripMenuItem.Name = "sachDienTuToolStripMenuItem";
            this.sachDienTuToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.sachDienTuToolStripMenuItem.Text = "Sách điện tử";
            this.sachDienTuToolStripMenuItem.Click += new System.EventHandler(this.sachDienTuToolStripMenuItem_Click);
            // 
            // nganTuToolStripMenuItem
            // 
            this.nganTuToolStripMenuItem.Name = "nganTuToolStripMenuItem";
            this.nganTuToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.nganTuToolStripMenuItem.Text = "Ngăn tủ";
            this.nganTuToolStripMenuItem.Click += new System.EventHandler(this.nganTuToolStripMenuItem_Click);
            // 
            // tacGiaToolStripMenuItem
            // 
            this.tacGiaToolStripMenuItem.Name = "tacGiaToolStripMenuItem";
            this.tacGiaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.tacGiaToolStripMenuItem.Text = "Tác giả";
            this.tacGiaToolStripMenuItem.Click += new System.EventHandler(this.tacGiaToolStripMenuItem_Click);
            // 
            // táchGiảSáchToolStripMenuItem
            // 
            this.táchGiảSáchToolStripMenuItem.Name = "táchGiảSáchToolStripMenuItem";
            this.táchGiảSáchToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.táchGiảSáchToolStripMenuItem.Text = "Tách giả - sách";
            // 
            // độcGiảToolStripMenuItem
            // 
            this.độcGiảToolStripMenuItem.Name = "độcGiảToolStripMenuItem";
            this.độcGiảToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.độcGiảToolStripMenuItem.Text = "Độc giả";
            this.độcGiảToolStripMenuItem.Click += new System.EventHandler(this.độcGiảToolStripMenuItem_Click);
            // 
            // nhanVienToolStripMenuItem
            // 
            this.nhanVienToolStripMenuItem.Name = "nhanVienToolStripMenuItem";
            this.nhanVienToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.nhanVienToolStripMenuItem.Text = "Nhân viên";
            this.nhanVienToolStripMenuItem.Click += new System.EventHandler(this.nhanVienToolStripMenuItem_Click);
            // 
            // phiếuMượnToolStripMenuItem
            // 
            this.phiếuMượnToolStripMenuItem.Name = "phiếuMượnToolStripMenuItem";
            this.phiếuMượnToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.phiếuMượnToolStripMenuItem.Text = "Phiếu mượn";
            // 
            // chiTiếtPhiếuMượnToolStripMenuItem
            // 
            this.chiTiếtPhiếuMượnToolStripMenuItem.Name = "chiTiếtPhiếuMượnToolStripMenuItem";
            this.chiTiếtPhiếuMượnToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.chiTiếtPhiếuMượnToolStripMenuItem.Text = "Chi tiết phiếu mượn";
            // 
            // quanLiTaiKhoanToolStripMenuItem
            // 
            this.quanLiTaiKhoanToolStripMenuItem.Name = "quanLiTaiKhoanToolStripMenuItem";
            this.quanLiTaiKhoanToolStripMenuItem.Size = new System.Drawing.Size(109, 20);
            this.quanLiTaiKhoanToolStripMenuItem.Text = "Quản lí tài khoản";
            this.quanLiTaiKhoanToolStripMenuItem.Click += new System.EventHandler(this.quanLiTaiKhoanToolStripMenuItem_Click);
            // 
            // mượnSáchToolStripMenuItem
            // 
            this.mượnSáchToolStripMenuItem.Name = "mượnSáchToolStripMenuItem";
            this.mượnSáchToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.mượnSáchToolStripMenuItem.Text = "Mượn sách";
            this.mượnSáchToolStripMenuItem.Click += new System.EventHandler(this.mượnSáchToolStripMenuItem_Click);
            // 
            // panelMain
            // 
            this.panelMain.Location = new System.Drawing.Point(10, 36);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(820, 526);
            this.panelMain.TabIndex = 2;
            // 
            // trảSáchToolStripMenuItem
            // 
            this.trảSáchToolStripMenuItem.Name = "trảSáchToolStripMenuItem";
            this.trảSáchToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.trảSáchToolStripMenuItem.Text = "Trả sách";
            this.trảSáchToolStripMenuItem.Click += new System.EventHandler(this.trảSáchToolStripMenuItem_Click);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 574);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.menuStrip1);
            this.Location = new System.Drawing.Point(0, 100);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainMenu";
            this.Text = "MainMenu";
            this.Load += new System.EventHandler(this.MainMenu_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.ToolStripMenuItem quanLiTaiKhoanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CapNhatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem theLoaiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ngonNguToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iSBNToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sáchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sachToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sachDienTuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nganTuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tacGiaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem táchGiảSáchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem độcGiảToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nhanVienToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem phiếuMượnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chiTiếtPhiếuMượnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mượnSáchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trảSáchToolStripMenuItem;
    }
}