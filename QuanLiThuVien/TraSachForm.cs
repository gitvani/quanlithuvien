﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLiThuVien
{
    public partial class TraSachForm : Form
    {
        public TraSachForm()
        {
            InitializeComponent();
        }

        private BindingSource maPhieuBindingSource ;
        private BindingSource sachDaChoMuonBindingSource;
        private BindingSource thongTinMuonBindingSource;
        private BindingSource lichSuMuonSachBindingSoure; 
        private const int soNgayMuonToDa = 30;
        private const int tienPhatMoiNgay = 500; 

        private void TraSachForm_Load(object sender, EventArgs e)
        {
            loadMaPhieuComboBoxData();
            loadMaSachComboBoxData();
            loadMaNhanVienData();
            loadNgayTraData();
            loadLichSuMuonSach(); 
        }

        private void loadLichSuMuonSach()
        {
            String sql = "EXEC LICHSUMUONSACH '" + maDocGiaTextBox.Text.Trim() + "'";
            lichSuMuonSachBindingSoure = Program.getBindingSource(sql);
            Console.WriteLine(sql); 
            myDataGridView.DataSource = lichSuMuonSachBindingSoure.DataSource;
        }

   
        private void loadNgayTraData()
        {
            ngayTraDateTimePicker.Value = DateTime.Now; 
        }

       

        private void loadMaPhieuComboBoxData()
        {
            maPhieuBindingSource = Program.getBindingSource("exec TRASACH_PHIEUCHUATRA");
            maPhieuComboBox.Items.Clear(); 
            for (int i = 0; i < maPhieuBindingSource.List.Count; i++)
            {
                DataRowView current = (DataRowView)maPhieuBindingSource.List[i];
                maPhieuComboBox.Items.Add(current["MAPHIEU"].ToString());

            }
        }
      
        private void loadMaSachComboBoxData()
        {
            sachDaChoMuonBindingSource = Program.getBindingSource("exec SACHDACHOMUON");
            maSachComboBox.Items.Clear();
            for (int i = 0; i < sachDaChoMuonBindingSource.List.Count; i++)
            {
                DataRowView current = (DataRowView) sachDaChoMuonBindingSource.List[i];
                maSachComboBox.Items.Add(current["MASACH"].ToString());

            }
        }
        private void loadMaNhanVienData()
        {
            maNhanVienTextBox.Text = Program.username; 
        }

        private void XemThongTinButton_Click(object sender, EventArgs e)
        {
            if (!getThongTinMuonSach())
            {
                saveButton.Enabled = false;
                tinhTrangTraComboBox.Enabled = false; 
                return; 
            }
            searchData();
            tinhTrangTraComboBox.Enabled = true; 
            tinhSoNgayQuaHanVaTienPhat();
            enableSaveButton(); 
            
        }

        private void xetPhatDoHongSach()
        {
            if (tinhTrangMuonComboBox.Text.Equals("Mới") && tinhTrangTraComboBox.Text.Equals("Cũ"))
            {
                hongSachTextBox.Text = "Có";
            }
            else
            {
                hongSachTextBox.Text = "Không";
            }
        }

        private void enableSaveButton()
        {
            saveButton.Enabled = true; 
        }



        private void tinhSoNgayQuaHanVaTienPhat()
        {
            TimeSpan timeSpan = ngayTraDateTimePicker.Value - ngayMuonDateTimePicker.Value;
            int soNgayQuaHan = (timeSpan.Days - soNgayMuonToDa < 0) ? 0 : timeSpan.Days - soNgayMuonToDa;
            soNgayQuanHanTextBox.Text = soNgayQuaHan.ToString();

            int tienPhat = soNgayQuaHan * tienPhatMoiNgay;
            TienPhatTextBox.Text = tienPhat.ToString(); 

        }

        private Boolean getThongTinMuonSach()
        {
            String sql = "EXEC TRASACH_XEMTHONGTIN '" + maPhieuComboBox.Text +"', '" + maSachComboBox.Text +"'" ; 
            Console.WriteLine(sql) ;
           thongTinMuonBindingSource =  Program.getBindingSource(sql);
           DataRowView current = (DataRowView)thongTinMuonBindingSource.List[0];
           if (current["TINHTRANGMUON"].ToString().Equals(""))
           {
               MessageBox.Show("Không tìm thấy thông tin. Hãy xem lại Mã phiếu và Mã sách.");
               return false;
           } 
           ngayMuonDateTimePicker.Value = DateTime.Parse(current["NGAYMUON"].ToString());
           tinhTrangMuonComboBox.SelectedIndex = (current["TINHTRANGMUON"].ToString().Equals("False")) ? 0: 1 ;
           return true; 
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (!checkEmptyValue())
            {
                return;
            }
            String sql = "EXEC TRASACH" + " '" + maPhieuComboBox.Text + "',"
                + " '" + maSachComboBox.Text + "',"
                + " '" + ngayTraDateTimePicker.Value.ToString("yyyy-MM-dd HH:mm:ss") + "',"
                + " '" + maNhanVienTextBox.Text + "',"
                + " '" + tinhTrangTraComboBox.SelectedIndex + "'";


                 Program.editData(sql);
                 loadLichSuMuonSach();
                 loadMaPhieuComboBoxData();
                 loadMaSachComboBoxData();
                 searchData();
                 tinhTrangTraComboBox.Enabled = false; 
                 //MessageBox.Show("Thành công"); 

        }

        private Boolean checkEmptyValue()
        {
            if (tinhTrangTraComboBox.Text.Trim().Equals(String.Empty))
            {
                MessageBox.Show("Tình trạng sách trả bị thiếu. Hãy nhập vào.", "Lỗi nhập liệu", MessageBoxButtons.OK);
                return false; 
            }
            if (tinhTrangTraComboBox.SelectedIndex < 0)
            {
                MessageBox.Show("Tình trạng sách không hợp lệ. Hãy nhập vào.", "Lỗi nhập liệu", MessageBoxButtons.OK);
                return false; 
            }
          
            return true; 
        }

        private void maPhieuComboBox_TextChanged(object sender, EventArgs e)
        {
            resetControllerEnableStatus();
        }
       

        private void maSachComboBox_TextChanged(object sender, EventArgs e)
        {
            resetControllerEnableStatus();
        }
        private void resetControllerEnableStatus()
        {
            saveButton.Enabled = false;
            tinhTrangTraComboBox.Enabled = false;
            if (searchData())
            {
                XemThongTinButton.Enabled = true;

            }
            else
            {
                XemThongTinButton.Enabled = false;
            }
        }
        private void FillterButton_Click(object sender, EventArgs e)
        {
            loadLichSuMuonSach();
        }

        private void myDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            
            foreach (DataGridViewRow row in myDataGridView.SelectedRows)
            {
                maPhieuComboBox.Text = row.Cells[0].Value.ToString();
                maSachComboBox.Text = row.Cells[1].Value.ToString();
                if (row.Cells[6].Value.ToString().Equals("False"))
                {
                   
                    XemThongTinButton.Enabled = true;
                }
                else
                {
                    XemThongTinButton.Enabled = false;
                }
               

            }
        }

        private  Boolean searchData()
        {
            //string searchValue = keyword.Trim();
            string maPhieu = maPhieuComboBox.Text.Trim();
            String maSach = maSachComboBox.Text.Trim();
            foreach (DataGridViewRow row in myDataGridView.Rows)
            {
                if(row.Cells[0].Value.ToString().Trim().Equals(maPhieu) &&
                    row.Cells[1].Value.ToString().Trim().Equals(maSach))
                {
                    myDataGridView.CurrentCell= row.Cells[0];
                 //   row.Selected = true;
                    return true; 
                }
            }
            return false; 
      
        }

        private void maDocGiaTextBox_TextChanged(object sender, EventArgs e)
        {
            loadLichSuMuonSach();
        }

        private void tinhTrangTraComboBox_TextChanged(object sender, EventArgs e)
        {
            xetPhatDoHongSach();
        }

        private void tinhTrangMuonComboBox_TextChanged(object sender, EventArgs e)
        {
            xetPhatDoHongSach();
        }
    }
}
