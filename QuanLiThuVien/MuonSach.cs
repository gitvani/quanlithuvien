﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLiThuVien
{
    public partial class MuonSach : Form
    {
        public MuonSach()
        {
            InitializeComponent();
        }

        private BindingSource docGiaBindingSource;
        private BindingSource sachBindingSource;
        private const int soSachMuonToDa = 3;
        private int soSachDuocMuon;
        private int soSachMuon;
        private void MuonSach_Load(object sender, EventArgs e)
        {
            // get data for  maDocGiaComboBox
            docGiaBindingSource = Program.getBindingSource("exec DOCGIA_XEM");

            for (int i = 0; i < docGiaBindingSource.List.Count; i++)
            {
                DataRowView current = (DataRowView)docGiaBindingSource.List[i];
                maDocGiaComboBox.Items.Add(current["MADG"].ToString());

            }
            // Get maNhanVien 
            maNhanVienTextBox.Text = Program.username; 
            // khởi tạo ngày mượn là today 
            ngayMuonDateTimePicker.Value = DateTime.Now;
            Console.WriteLine(ngayMuonDateTimePicker.Value); 
            // get maSach 
            loadMaSachComboBox(); 
            

        }
        private void loadMaSachComboBox()
        {
            sachBindingSource = Program.getBindingSource("exec SACHCHUAMUON");
            maSach1ComboBox.Items.Clear();
            maSach2ComboBox.Items.Clear(); 
            maSach3ComboBox.Items.Clear(); 
            for (int i = 0; i < sachBindingSource.List.Count; i++)
            {
                DataRowView current = (DataRowView)sachBindingSource.List[i];
                maSach1ComboBox.Items.Add(current["MASACH"].ToString());
                maSach2ComboBox.Items.Add(current["MASACH"].ToString());
                maSach3ComboBox.Items.Add(current["MASACH"].ToString());
            }
        }
        private void setDataForDataGridView()
        {
            BindingSource lichSuMuonSachBindingSource = Program.getBindingSource("exec LICHSUMUONSACH " + maDocGiaComboBox.Text);
            myDataGridView.DataSource = lichSuMuonSachBindingSource.DataSource;
           
        }
        private void maDocComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
           // lay thong tin lich su muon sach
            if (maDocGiaComboBox.SelectedIndex < 0)
            {
                return;
            }
            setDataForDataGridView(); 

            /////////////////////
            DataRowView currentRow = (DataRowView)docGiaBindingSource.List[maDocGiaComboBox.SelectedIndex];
            tenDocGiaTextBox.Text = currentRow["TENDG"].ToString();

            // tính số sách còn có thể mượn 
            BindingSource soSachChuaTraBindingSource = Program.getBindingSource("exec SOSACHCHUATRA " +maDocGiaComboBox.Text);


             DataRowView current = (DataRowView)soSachChuaTraBindingSource.List[0];
             int soSachChuaTra = Int32.Parse(current["SOSACHCHUATRA"].ToString());
               soSachDuocMuon = soSachMuonToDa - soSachChuaTra ;
               if (soSachDuocMuon < 0)
               {
                   soSachDuocMuon = 0; 
               }
            SoSachDuocMuonTextBox.Text = soSachDuocMuon.ToString();

        }

        private void SoSachDuocMuonTextBox_TextChanged(object sender, EventArgs e)
        {
            maSach1ComboBox.Enabled = false;
            maSach2ComboBox.Enabled = false;
            maSach3ComboBox.Enabled = false;
            tinhTrangSach1ComboBox.Enabled = false;
            tinhTrangSach2ComboBox.Enabled = false;
            tinhTrangSach3ComboBox.Enabled = false;
            hinhThucComboBox.Enabled = false; 
            saveButton.Enabled = false;
            soSachMuonComboBox.Enabled = false;
            if (soSachDuocMuon >= 1)
            {            
                soSachMuonComboBox.Enabled = true; 
            }
         
             soSachMuonComboBox.Items.Clear(); 
             for (int i = 1; i <= soSachDuocMuon; i++)
             {
                 soSachMuonComboBox.Items.Add(i); 
             }
        }
        private void capNhatSach()
        {
            if (soSachMuon >= 1)
            {
                Program.editData("exec SACH_CAPNHATCHOMUON '" + maSach1ComboBox.Text + "', " + 1);
                Console.WriteLine("exec SACH_CAPNHATCHOMUON '" + maSach1ComboBox.Text + "', " + 1); 
            }
            if (soSachMuon >= 2)
            {
                Program.editData("exec SACH_CAPNHATCHOMUON '" + maSach2ComboBox.Text + "', " + 1);
            }
            if (soSachMuon >= 3)
            {
                Program.editData("exec SACH_CAPNHATCHOMUON '" + maSach3ComboBox.Text + "', " + 1);
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (!checkEmptyValue())
            {
                return; 
            } 
           
            String maDG = maDocGiaComboBox.Text;
            String hinhThuc = hinhThucComboBox.SelectedIndex.ToString();
            String ngayMuon = ngayMuonDateTimePicker.Value.ToString("yyyy-MM-dd HH:mm:ss");
            String maNV = maNhanVienTextBox.Text;
            String maSach1 = maSach1ComboBox.Text; 
            String maSach2 = maSach2ComboBox.Text ;
            String maSach3 = maSach3ComboBox.Text;
            String tinhTrangMuon1 = (tinhTrangSach1ComboBox.SelectedIndex == -1) ? "" : tinhTrangSach1ComboBox.SelectedIndex.ToString();
            String tinhTrangMuon2 = (tinhTrangSach2ComboBox.SelectedIndex == -1) ? "" : tinhTrangSach2ComboBox.SelectedIndex.ToString();
            String tinhTrangMuon3 = (tinhTrangSach3ComboBox.SelectedIndex == -1) ? "" : tinhTrangSach3ComboBox.SelectedIndex.ToString(); 
          

            String sql = "exec MUONSACH "
                + " '" + maDG + "', "
                  + " '" + hinhThuc + "', "
                    + " '" + ngayMuon + "', "
                      + " '" + maNV + "', "
                        + " '" + maSach1 + "', "
                          + " '" + maSach2 + "', "
                            + " '" + maSach3 + "', "
                              + " '" + tinhTrangMuon1 + "', "
                                + " '" + tinhTrangMuon2 + "', "
                                  + " '" + tinhTrangMuon3 + "', "
                                    + " '" + soSachMuon + "' ";
            Console.WriteLine(sql);
            try
            {
                Program.editData(sql);
               
               // cập nhật gridview
                setDataForDataGridView();
            
                // cập nhật lại trình trạng sách CHOMUON = 1
                capNhatSach();
                loadMaSachComboBox();

                // reset
                resetControllerData(); 
              
              
            }
            catch  (Exception ex){
                MessageBox.Show("Lỗi: " + ex.Message, "Lỗi", MessageBoxButtons.OK); 
            }
          
        }
        private void resetControllerData() {
            maDocGiaComboBox.Text = "";
            soSachMuonComboBox.Text = "";
            maSach1ComboBox.Text = "";
            maSach2ComboBox.Text = "";
            maSach3ComboBox.Text = "";
            tinhTrangSach1ComboBox.Text = "";
            tinhTrangSach2ComboBox.Text = "";
            tinhTrangSach3ComboBox.Text = "";
            hinhThucComboBox.Text = "";
            soSachMuonComboBox.Enabled = false;
            hinhThucComboBox.Enabled = false;
            maSach1ComboBox.Enabled = false;
            maSach2ComboBox.Enabled = false;
            maSach3ComboBox.Enabled = false;
            tinhTrangSach1ComboBox.Enabled = false;
            tinhTrangSach2ComboBox.Enabled = false;
            tinhTrangSach3ComboBox.Enabled = false;
            saveButton.Enabled = false;
            
          
        }
        private Boolean checkEmptyValue()
        {
            if (maDocGiaComboBox.Text == String.Empty)
            {
                MessageBox.Show("Mã độc giả không thể thiếu", "Lỗi nhập liệu", MessageBoxButtons.OK);
                return false; 
            }
            if (hinhThucComboBox.Text == String.Empty)
            {
                MessageBox.Show("Hình thức không thể thiếu", "Lỗi nhập liệu", MessageBoxButtons.OK);
                return false; 
            }

            if (ngayMuonDateTimePicker.Value.ToString() == String.Empty)
            {
                MessageBox.Show("Ngày mượn không thể thiếu", "Lỗi nhập liệu", MessageBoxButtons.OK);
                return false;
            }
            //if (ngayMuonDateTimePicker.Value.CompareTo(DateTime.Today)> 0)
            //{
            //    MessageBox.Show("Ngày mượn không thể trể hơn hôm nay " + DateTime.Today.ToString(), "Lỗi nhập liệu", MessageBoxButtons.OK);
            //    return;
            //}
           
            if (soSachMuon >= 1)
            {
                if (maSach1ComboBox.Text == String.Empty)
                {
                    MessageBox.Show("Mã sách 1 không thể thiếu", "Lỗi nhập liệu", MessageBoxButtons.OK);
                    return false;
                }
                if (tinhTrangSach1ComboBox.Text == String.Empty)
                {
                    MessageBox.Show("Tình trạng sách 1 không thể thiếu", "Lỗi nhập liệu", MessageBoxButtons.OK);
                    return false;
                }
            }

            if (soSachMuon >= 2)
            {
                if (maSach2ComboBox.Text == String.Empty)
                {
                    MessageBox.Show("Mã sách 2 không thể thiếu", "Lỗi nhập liệu", MessageBoxButtons.OK);
                    return false;
                }
                if (tinhTrangSach2ComboBox.Text == String.Empty)
                {
                    MessageBox.Show("Tình trạng sách 2 không thể thiếu", "Lỗi nhập liệu", MessageBoxButtons.OK);
                    return false;
                }

                if (maSach1ComboBox.Text.Equals(maSach2ComboBox.Text))
                {
                    MessageBox.Show("Các mã sách 1 và 2 trùng nhau", "Lỗi nhập liệu", MessageBoxButtons.OK);
                    return false;
                }
            }

            if (soSachMuon >= 3)
            {
                if (maSach3ComboBox.Text == String.Empty)
                {
                    MessageBox.Show("Mã sách 3 không thể thiếu", "Lỗi nhập liệu", MessageBoxButtons.OK);
                    return false;
                }
                if (tinhTrangSach3ComboBox.Text == String.Empty)
                {
                    MessageBox.Show("Tình trạng sách 3 không thể thiếu", "Lỗi nhập liệu", MessageBoxButtons.OK);
                    return false;
                }

                if (maSach2ComboBox.Text.Equals(maSach3ComboBox.Text))
                {
                    MessageBox.Show("Các mã sách 2 và 3 trùng nhau", "Lỗi nhập liệu", MessageBoxButtons.OK);
                    return false;
                }
                else if (maSach1ComboBox.Text.Equals(maSach3ComboBox.Text))
                {
                    MessageBox.Show("Các mã sách 1 và 3 trùng nhau", "Lỗi nhập liệu", MessageBoxButtons.OK);
                    return false;
                } 
            }
           
            return true; 
        }

        private void soSachMuonComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (soSachMuonComboBox.SelectedIndex < 0)
            {
                return;
            }
            soSachMuon = Int32.Parse(soSachMuonComboBox.Text); 
            maSach1ComboBox.Enabled = false;
            maSach2ComboBox.Enabled = false;
            maSach3ComboBox.Enabled = false;
            tinhTrangSach1ComboBox.Enabled = false;
            tinhTrangSach2ComboBox.Enabled = false;
            tinhTrangSach3ComboBox.Enabled = false;
            saveButton.Enabled = false;
          
         
            if (soSachMuon >= 1)
            {
                maSach1ComboBox.Enabled = true;
                tinhTrangSach1ComboBox.Enabled = true;
                saveButton.Enabled = true;
                hinhThucComboBox.Enabled = true; 
            }
            if (soSachMuon >= 2)
            {
                maSach2ComboBox.Enabled = true;
                tinhTrangSach2ComboBox.Enabled = true;

            }
            if (soSachMuon >= 3)
            {
                maSach3ComboBox.Enabled = true;
                tinhTrangSach3ComboBox.Enabled = true;
            }

        }
    }
}
