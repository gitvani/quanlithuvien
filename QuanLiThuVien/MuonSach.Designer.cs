﻿namespace QuanLiThuVien
{
    partial class MuonSach
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.maDocGiaComboBox = new System.Windows.Forms.ComboBox();
            this.tenDocGiaTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.hinhThucComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ngayMuonDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.maNhanVienTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.maSach1ComboBox = new System.Windows.Forms.ComboBox();
            this.maSach2ComboBox = new System.Windows.Forms.ComboBox();
            this.maSach3ComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tinhTrangSach1ComboBox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.SoSachDuocMuonTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tinhTrangSach2ComboBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tinhTrangSach3ComboBox = new System.Windows.Forms.ComboBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.soSachMuonComboBox = new System.Windows.Forms.ComboBox();
            this.myDataGridView = new System.Windows.Forms.DataGridView();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.myDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 221);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã độc giả:";
            // 
            // maDocGiaComboBox
            // 
            this.maDocGiaComboBox.FormattingEnabled = true;
            this.maDocGiaComboBox.Location = new System.Drawing.Point(118, 218);
            this.maDocGiaComboBox.Name = "maDocGiaComboBox";
            this.maDocGiaComboBox.Size = new System.Drawing.Size(121, 21);
            this.maDocGiaComboBox.TabIndex = 1;
            this.maDocGiaComboBox.SelectedIndexChanged += new System.EventHandler(this.maDocComboBox_SelectedIndexChanged);
            // 
            // tenDocGiaTextBox
            // 
            this.tenDocGiaTextBox.Enabled = false;
            this.tenDocGiaTextBox.Location = new System.Drawing.Point(118, 258);
            this.tenDocGiaTextBox.Name = "tenDocGiaTextBox";
            this.tenDocGiaTextBox.Size = new System.Drawing.Size(121, 20);
            this.tenDocGiaTextBox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 262);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Tên độc giả:";
            // 
            // hinhThucComboBox
            // 
            this.hinhThucComboBox.Enabled = false;
            this.hinhThucComboBox.FormattingEnabled = true;
            this.hinhThucComboBox.Items.AddRange(new object[] {
            "Mượn tại chỗ",
            "Mang về"});
            this.hinhThucComboBox.Location = new System.Drawing.Point(672, 217);
            this.hinhThucComboBox.Name = "hinhThucComboBox";
            this.hinhThucComboBox.Size = new System.Drawing.Size(139, 21);
            this.hinhThucComboBox.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(610, 221);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Hình thức:";
            // 
            // ngayMuonDateTimePicker
            // 
            this.ngayMuonDateTimePicker.Enabled = false;
            this.ngayMuonDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ngayMuonDateTimePicker.Location = new System.Drawing.Point(118, 285);
            this.ngayMuonDateTimePicker.Name = "ngayMuonDateTimePicker";
            this.ngayMuonDateTimePicker.Size = new System.Drawing.Size(121, 20);
            this.ngayMuonDateTimePicker.TabIndex = 72;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(36, 288);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 71;
            this.label7.Text = "Ngày mượn:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 331);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 73;
            this.label4.Text = "Mã nhân viên:";
            // 
            // maNhanVienTextBox
            // 
            this.maNhanVienTextBox.Enabled = false;
            this.maNhanVienTextBox.Location = new System.Drawing.Point(118, 328);
            this.maNhanVienTextBox.Name = "maNhanVienTextBox";
            this.maNhanVienTextBox.Size = new System.Drawing.Size(121, 20);
            this.maNhanVienTextBox.TabIndex = 74;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 75;
            this.label5.Text = "Quyển 1:";
            // 
            // maSach1ComboBox
            // 
            this.maSach1ComboBox.Enabled = false;
            this.maSach1ComboBox.FormattingEnabled = true;
            this.maSach1ComboBox.Location = new System.Drawing.Point(88, 28);
            this.maSach1ComboBox.Name = "maSach1ComboBox";
            this.maSach1ComboBox.Size = new System.Drawing.Size(121, 21);
            this.maSach1ComboBox.TabIndex = 76;
            // 
            // maSach2ComboBox
            // 
            this.maSach2ComboBox.Enabled = false;
            this.maSach2ComboBox.FormattingEnabled = true;
            this.maSach2ComboBox.Location = new System.Drawing.Point(88, 55);
            this.maSach2ComboBox.Name = "maSach2ComboBox";
            this.maSach2ComboBox.Size = new System.Drawing.Size(121, 21);
            this.maSach2ComboBox.TabIndex = 77;
            // 
            // maSach3ComboBox
            // 
            this.maSach3ComboBox.Enabled = false;
            this.maSach3ComboBox.FormattingEnabled = true;
            this.maSach3ComboBox.Location = new System.Drawing.Point(88, 82);
            this.maSach3ComboBox.Name = "maSach3ComboBox";
            this.maSach3ComboBox.Size = new System.Drawing.Size(121, 21);
            this.maSach3ComboBox.TabIndex = 78;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.tinhTrangSach3ComboBox);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.tinhTrangSach2ComboBox);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tinhTrangSach1ComboBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.maSach3ComboBox);
            this.groupBox1.Controls.Add(this.maSach1ComboBox);
            this.groupBox1.Controls.Add(this.maSach2ComboBox);
            this.groupBox1.Location = new System.Drawing.Point(274, 247);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(537, 129);
            this.groupBox1.TabIndex = 79;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Mã sách:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 79;
            this.label6.Text = "Quyển 2:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 80;
            this.label8.Text = "Quyển 3:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(238, 36);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 13);
            this.label10.TabIndex = 83;
            this.label10.Text = "Tình trạng sách:";
            // 
            // tinhTrangSach1ComboBox
            // 
            this.tinhTrangSach1ComboBox.Enabled = false;
            this.tinhTrangSach1ComboBox.FormattingEnabled = true;
            this.tinhTrangSach1ComboBox.Items.AddRange(new object[] {
            "Cũ",
            "Mới"});
            this.tinhTrangSach1ComboBox.Location = new System.Drawing.Point(338, 33);
            this.tinhTrangSach1ComboBox.Name = "tinhTrangSach1ComboBox";
            this.tinhTrangSach1ComboBox.Size = new System.Drawing.Size(121, 21);
            this.tinhTrangSach1ComboBox.TabIndex = 82;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(271, 221);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(132, 13);
            this.label11.TabIndex = 84;
            this.label11.Text = "Số sách còn có thể mượn:";
            // 
            // SoSachDuocMuonTextBox
            // 
            this.SoSachDuocMuonTextBox.Enabled = false;
            this.SoSachDuocMuonTextBox.Location = new System.Drawing.Point(409, 218);
            this.SoSachDuocMuonTextBox.Name = "SoSachDuocMuonTextBox";
            this.SoSachDuocMuonTextBox.Size = new System.Drawing.Size(35, 20);
            this.SoSachDuocMuonTextBox.TabIndex = 85;
            this.SoSachDuocMuonTextBox.TextChanged += new System.EventHandler(this.SoSachDuocMuonTextBox_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(238, 63);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 13);
            this.label12.TabIndex = 85;
            this.label12.Text = "Tình trạng sách:";
            // 
            // tinhTrangSach2ComboBox
            // 
            this.tinhTrangSach2ComboBox.Enabled = false;
            this.tinhTrangSach2ComboBox.FormattingEnabled = true;
            this.tinhTrangSach2ComboBox.Items.AddRange(new object[] {
            "Cũ",
            "Mới"});
            this.tinhTrangSach2ComboBox.Location = new System.Drawing.Point(338, 60);
            this.tinhTrangSach2ComboBox.Name = "tinhTrangSach2ComboBox";
            this.tinhTrangSach2ComboBox.Size = new System.Drawing.Size(121, 21);
            this.tinhTrangSach2ComboBox.TabIndex = 84;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(238, 90);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 13);
            this.label13.TabIndex = 87;
            this.label13.Text = "Tình trạng sách:";
            // 
            // tinhTrangSach3ComboBox
            // 
            this.tinhTrangSach3ComboBox.Enabled = false;
            this.tinhTrangSach3ComboBox.FormattingEnabled = true;
            this.tinhTrangSach3ComboBox.Items.AddRange(new object[] {
            "Cũ",
            "Mới"});
            this.tinhTrangSach3ComboBox.Location = new System.Drawing.Point(338, 87);
            this.tinhTrangSach3ComboBox.Name = "tinhTrangSach3ComboBox";
            this.tinhTrangSach3ComboBox.Size = new System.Drawing.Size(121, 21);
            this.tinhTrangSach3ComboBox.TabIndex = 86;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(601, 393);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(210, 27);
            this.saveButton.TabIndex = 86;
            this.saveButton.Text = "Lập phiếu mượn";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(450, 221);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 13);
            this.label9.TabIndex = 87;
            this.label9.Text = "Số sách mượn:";
            // 
            // soSachMuonComboBox
            // 
            this.soSachMuonComboBox.Enabled = false;
            this.soSachMuonComboBox.FormattingEnabled = true;
            this.soSachMuonComboBox.Items.AddRange(new object[] {
            "0"});
            this.soSachMuonComboBox.Location = new System.Drawing.Point(534, 217);
            this.soSachMuonComboBox.Name = "soSachMuonComboBox";
            this.soSachMuonComboBox.Size = new System.Drawing.Size(60, 21);
            this.soSachMuonComboBox.TabIndex = 88;
            this.soSachMuonComboBox.SelectedIndexChanged += new System.EventHandler(this.soSachMuonComboBox_SelectedIndexChanged);
            // 
            // myDataGridView
            // 
            this.myDataGridView.AllowUserToAddRows = false;
            this.myDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.myDataGridView.Location = new System.Drawing.Point(29, 35);
            this.myDataGridView.Name = "myDataGridView";
            this.myDataGridView.ReadOnly = true;
            this.myDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.myDataGridView.Size = new System.Drawing.Size(787, 150);
            this.myDataGridView.TabIndex = 89;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(28, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(99, 13);
            this.label14.TabIndex = 90;
            this.label14.Text = "Lịch sử mượn sách:";
            // 
            // MuonSach
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 510);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.myDataGridView);
            this.Controls.Add(this.soSachMuonComboBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.SoSachDuocMuonTextBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.maNhanVienTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ngayMuonDateTimePicker);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.hinhThucComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tenDocGiaTextBox);
            this.Controls.Add(this.maDocGiaComboBox);
            this.Controls.Add(this.label1);
            this.Name = "MuonSach";
            this.Text = "MuonSach";
            this.Load += new System.EventHandler(this.MuonSach_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.myDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox maDocGiaComboBox;
        private System.Windows.Forms.TextBox tenDocGiaTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox hinhThucComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker ngayMuonDateTimePicker;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox maNhanVienTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox maSach1ComboBox;
        private System.Windows.Forms.ComboBox maSach2ComboBox;
        private System.Windows.Forms.ComboBox maSach3ComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox tinhTrangSach1ComboBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox SoSachDuocMuonTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox tinhTrangSach2ComboBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox tinhTrangSach3ComboBox;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox soSachMuonComboBox;
        private System.Windows.Forms.DataGridView myDataGridView;
        private System.Windows.Forms.Label label14;
    }
}