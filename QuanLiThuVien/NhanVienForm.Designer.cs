﻿namespace QuanLiThuVien
{
    partial class NhanVienForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mySearchButton = new System.Windows.Forms.Button();
            this.mySearchTextBox = new System.Windows.Forms.TextBox();
            this.hoNhanVienTextBox = new System.Windows.Forms.TextBox();
            this.tenNgonNguLabel = new System.Windows.Forms.Label();
            this.maNhanVienTextBox = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.myDataGridView = new System.Windows.Forms.DataGridView();
            this.moveFirstButton = new System.Windows.Forms.Button();
            this.moveNextButton = new System.Windows.Forms.Button();
            this.MoveLastButton = new System.Windows.Forms.Button();
            this.movePreviousButton = new System.Windows.Forms.Button();
            this.myAddButton = new System.Windows.Forms.Button();
            this.myCancelButton = new System.Windows.Forms.Button();
            this.mySaveButton = new System.Windows.Forms.Button();
            this.myEditButton = new System.Windows.Forms.Button();
            this.myDeleteButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tenNhanVienTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.diaChiTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dienThoaiTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.gioiTinhComboBox = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.myDataGridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mySearchButton
            // 
            this.mySearchButton.Location = new System.Drawing.Point(601, 28);
            this.mySearchButton.Name = "mySearchButton";
            this.mySearchButton.Size = new System.Drawing.Size(75, 23);
            this.mySearchButton.TabIndex = 34;
            this.mySearchButton.Text = "Tìm kiếm";
            this.mySearchButton.UseVisualStyleBackColor = true;
            this.mySearchButton.Click += new System.EventHandler(this.mySearchButton_Click);
            // 
            // mySearchTextBox
            // 
            this.mySearchTextBox.Location = new System.Drawing.Point(468, 31);
            this.mySearchTextBox.Name = "mySearchTextBox";
            this.mySearchTextBox.Size = new System.Drawing.Size(127, 20);
            this.mySearchTextBox.TabIndex = 33;
            this.mySearchTextBox.Click += new System.EventHandler(this.mySearchButton_Click);
            // 
            // hoNhanVienTextBox
            // 
            this.hoNhanVienTextBox.Enabled = false;
            this.hoNhanVienTextBox.Location = new System.Drawing.Point(79, 315);
            this.hoNhanVienTextBox.Name = "hoNhanVienTextBox";
            this.hoNhanVienTextBox.Size = new System.Drawing.Size(71, 20);
            this.hoNhanVienTextBox.TabIndex = 32;
            // 
            // tenNgonNguLabel
            // 
            this.tenNgonNguLabel.AutoSize = true;
            this.tenNgonNguLabel.Location = new System.Drawing.Point(49, 318);
            this.tenNgonNguLabel.Name = "tenNgonNguLabel";
            this.tenNgonNguLabel.Size = new System.Drawing.Size(24, 13);
            this.tenNgonNguLabel.TabIndex = 31;
            this.tenNgonNguLabel.Text = "Họ:";
            // 
            // maNhanVienTextBox
            // 
            this.maNhanVienTextBox.Enabled = false;
            this.maNhanVienTextBox.Location = new System.Drawing.Point(120, 276);
            this.maNhanVienTextBox.Name = "maNhanVienTextBox";
            this.maNhanVienTextBox.Size = new System.Drawing.Size(149, 20);
            this.maNhanVienTextBox.TabIndex = 30;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(49, 279);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(75, 13);
            this.label.TabIndex = 29;
            this.label.Text = "Mã nhân viên:";
            // 
            // myDataGridView
            // 
            this.myDataGridView.AllowUserToAddRows = false;
            this.myDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.myDataGridView.Location = new System.Drawing.Point(52, 57);
            this.myDataGridView.Name = "myDataGridView";
            this.myDataGridView.ReadOnly = true;
            this.myDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.myDataGridView.Size = new System.Drawing.Size(624, 150);
            this.myDataGridView.TabIndex = 28;
            this.myDataGridView.SelectionChanged += new System.EventHandler(this.myDataGridView_SelectionChanged);
            // 
            // moveFirstButton
            // 
            this.moveFirstButton.Location = new System.Drawing.Point(6, 11);
            this.moveFirstButton.Name = "moveFirstButton";
            this.moveFirstButton.Size = new System.Drawing.Size(30, 23);
            this.moveFirstButton.TabIndex = 11;
            this.moveFirstButton.Text = "<<";
            this.moveFirstButton.UseVisualStyleBackColor = true;
            this.moveFirstButton.Click += new System.EventHandler(this.moveFirstButton_Click);
            // 
            // moveNextButton
            // 
            this.moveNextButton.Location = new System.Drawing.Point(78, 11);
            this.moveNextButton.Name = "moveNextButton";
            this.moveNextButton.Size = new System.Drawing.Size(30, 23);
            this.moveNextButton.TabIndex = 14;
            this.moveNextButton.Text = ">";
            this.moveNextButton.UseVisualStyleBackColor = true;
            this.moveNextButton.Click += new System.EventHandler(this.moveNextButton_Click);
            // 
            // MoveLastButton
            // 
            this.MoveLastButton.Location = new System.Drawing.Point(114, 11);
            this.MoveLastButton.Name = "MoveLastButton";
            this.MoveLastButton.Size = new System.Drawing.Size(30, 23);
            this.MoveLastButton.TabIndex = 12;
            this.MoveLastButton.Text = ">>";
            this.MoveLastButton.UseVisualStyleBackColor = true;
            this.MoveLastButton.Click += new System.EventHandler(this.MoveLastButton_Click);
            // 
            // movePreviousButton
            // 
            this.movePreviousButton.Location = new System.Drawing.Point(42, 11);
            this.movePreviousButton.Name = "movePreviousButton";
            this.movePreviousButton.Size = new System.Drawing.Size(30, 23);
            this.movePreviousButton.TabIndex = 13;
            this.movePreviousButton.Text = "<";
            this.movePreviousButton.UseVisualStyleBackColor = true;
            this.movePreviousButton.Click += new System.EventHandler(this.movePreviousButton_Click);
            // 
            // myAddButton
            // 
            this.myAddButton.Location = new System.Drawing.Point(6, 19);
            this.myAddButton.Name = "myAddButton";
            this.myAddButton.Size = new System.Drawing.Size(100, 23);
            this.myAddButton.TabIndex = 6;
            this.myAddButton.Text = "Thêm";
            this.myAddButton.UseVisualStyleBackColor = true;
            this.myAddButton.Click += new System.EventHandler(this.myAddButton_Click);
            // 
            // myCancelButton
            // 
            this.myCancelButton.Enabled = false;
            this.myCancelButton.Location = new System.Drawing.Point(162, 49);
            this.myCancelButton.Name = "myCancelButton";
            this.myCancelButton.Size = new System.Drawing.Size(100, 23);
            this.myCancelButton.TabIndex = 17;
            this.myCancelButton.Text = "Bỏ qua";
            this.myCancelButton.UseVisualStyleBackColor = true;
            this.myCancelButton.Click += new System.EventHandler(this.myCancelButton_Click);
            // 
            // mySaveButton
            // 
            this.mySaveButton.Enabled = false;
            this.mySaveButton.Location = new System.Drawing.Point(56, 50);
            this.mySaveButton.Name = "mySaveButton";
            this.mySaveButton.Size = new System.Drawing.Size(100, 23);
            this.mySaveButton.TabIndex = 16;
            this.mySaveButton.Text = "Ghi";
            this.mySaveButton.UseVisualStyleBackColor = true;
            this.mySaveButton.Click += new System.EventHandler(this.mySaveButton_Click);
            // 
            // myEditButton
            // 
            this.myEditButton.Location = new System.Drawing.Point(118, 19);
            this.myEditButton.Name = "myEditButton";
            this.myEditButton.Size = new System.Drawing.Size(100, 23);
            this.myEditButton.TabIndex = 8;
            this.myEditButton.Text = "Sửa";
            this.myEditButton.UseVisualStyleBackColor = true;
            this.myEditButton.Click += new System.EventHandler(this.myEditButton_Click);
            // 
            // myDeleteButton
            // 
            this.myDeleteButton.Location = new System.Drawing.Point(230, 19);
            this.myDeleteButton.Name = "myDeleteButton";
            this.myDeleteButton.Size = new System.Drawing.Size(100, 23);
            this.myDeleteButton.TabIndex = 7;
            this.myDeleteButton.Text = "Xóa";
            this.myDeleteButton.UseVisualStyleBackColor = true;
            this.myDeleteButton.Click += new System.EventHandler(this.myDeleteButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.myAddButton);
            this.groupBox2.Controls.Add(this.myCancelButton);
            this.groupBox2.Controls.Add(this.mySaveButton);
            this.groupBox2.Controls.Add(this.myEditButton);
            this.groupBox2.Controls.Add(this.myDeleteButton);
            this.groupBox2.Location = new System.Drawing.Point(336, 224);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(340, 83);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.moveFirstButton);
            this.groupBox1.Controls.Add(this.moveNextButton);
            this.groupBox1.Controls.Add(this.MoveLastButton);
            this.groupBox1.Controls.Add(this.movePreviousButton);
            this.groupBox1.Location = new System.Drawing.Point(52, 213);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(149, 44);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            // 
            // tenNhanVienTextBox
            // 
            this.tenNhanVienTextBox.Enabled = false;
            this.tenNhanVienTextBox.Location = new System.Drawing.Point(193, 315);
            this.tenNhanVienTextBox.Name = "tenNhanVienTextBox";
            this.tenNhanVienTextBox.Size = new System.Drawing.Size(76, 20);
            this.tenNhanVienTextBox.TabIndex = 38;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(163, 318);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 37;
            this.label1.Text = "Tên:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(49, 352);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 39;
            this.label2.Text = "Giới tính:";
            // 
            // diaChiTextBox
            // 
            this.diaChiTextBox.Enabled = false;
            this.diaChiTextBox.Location = new System.Drawing.Point(120, 387);
            this.diaChiTextBox.Name = "diaChiTextBox";
            this.diaChiTextBox.Size = new System.Drawing.Size(149, 20);
            this.diaChiTextBox.TabIndex = 42;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(49, 390);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 41;
            this.label3.Text = "Địa chỉ:";
            // 
            // emailTextBox
            // 
            this.emailTextBox.Enabled = false;
            this.emailTextBox.Location = new System.Drawing.Point(120, 440);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(149, 20);
            this.emailTextBox.TabIndex = 44;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 443);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 43;
            this.label4.Text = "Email:";
            // 
            // dienThoaiTextBox
            // 
            this.dienThoaiTextBox.Enabled = false;
            this.dienThoaiTextBox.Location = new System.Drawing.Point(120, 413);
            this.dienThoaiTextBox.Name = "dienThoaiTextBox";
            this.dienThoaiTextBox.Size = new System.Drawing.Size(149, 20);
            this.dienThoaiTextBox.TabIndex = 46;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(49, 416);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 45;
            this.label5.Text = "Điện thoại:";
            // 
            // gioiTinhComboBox
            // 
            this.gioiTinhComboBox.Enabled = false;
            this.gioiTinhComboBox.FormattingEnabled = true;
            this.gioiTinhComboBox.Items.AddRange(new object[] {
            "Nữ",
            "Nam"});
            this.gioiTinhComboBox.Location = new System.Drawing.Point(120, 349);
            this.gioiTinhComboBox.Name = "gioiTinhComboBox";
            this.gioiTinhComboBox.Size = new System.Drawing.Size(149, 21);
            this.gioiTinhComboBox.TabIndex = 47;
            // 
            // NhanVienForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(757, 475);
            this.Controls.Add(this.gioiTinhComboBox);
            this.Controls.Add(this.dienThoaiTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.diaChiTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tenNhanVienTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.mySearchButton);
            this.Controls.Add(this.mySearchTextBox);
            this.Controls.Add(this.hoNhanVienTextBox);
            this.Controls.Add(this.tenNgonNguLabel);
            this.Controls.Add(this.maNhanVienTextBox);
            this.Controls.Add(this.label);
            this.Controls.Add(this.myDataGridView);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "NhanVienForm";
            this.Text = "NhanVienForm";
            this.Load += new System.EventHandler(this.myForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.myDataGridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button mySearchButton;
        private System.Windows.Forms.TextBox mySearchTextBox;
        private System.Windows.Forms.TextBox hoNhanVienTextBox;
        private System.Windows.Forms.Label tenNgonNguLabel;
        private System.Windows.Forms.TextBox maNhanVienTextBox;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.DataGridView myDataGridView;
        private System.Windows.Forms.Button moveFirstButton;
        private System.Windows.Forms.Button moveNextButton;
        private System.Windows.Forms.Button MoveLastButton;
        private System.Windows.Forms.Button movePreviousButton;
        private System.Windows.Forms.Button myAddButton;
        private System.Windows.Forms.Button myCancelButton;
        private System.Windows.Forms.Button mySaveButton;
        private System.Windows.Forms.Button myEditButton;
        private System.Windows.Forms.Button myDeleteButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tenNhanVienTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox diaChiTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox dienThoaiTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox gioiTinhComboBox;
    }
}