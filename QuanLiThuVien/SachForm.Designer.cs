﻿namespace QuanLiThuVien
{
    partial class SachForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.moTaTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tenSachTextBox = new System.Windows.Forms.TextBox();
            this.maNganTuComboBox = new System.Windows.Forms.ComboBox();
            this.ISBNComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.moveFirstButton = new System.Windows.Forms.Button();
            this.moveNextButton = new System.Windows.Forms.Button();
            this.MoveLastButton = new System.Windows.Forms.Button();
            this.movePreviousButton = new System.Windows.Forms.Button();
            this.mySearchButton = new System.Windows.Forms.Button();
            this.mySearchTextBox = new System.Windows.Forms.TextBox();
            this.maSachTextBox = new System.Windows.Forms.TextBox();
            this.label100 = new System.Windows.Forms.Label();
            this.myDataGridView = new System.Windows.Forms.DataGridView();
            this.myAddButton = new System.Windows.Forms.Button();
            this.myCancelButton = new System.Windows.Forms.Button();
            this.myEditButton = new System.Windows.Forms.Button();
            this.myDeleteButton = new System.Windows.Forms.Button();
            this.mySaveButton = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tinhTrangComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.choMuonComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.myDataGridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // moTaTextBox
            // 
            this.moTaTextBox.Enabled = false;
            this.moTaTextBox.Location = new System.Drawing.Point(330, 440);
            this.moTaTextBox.Name = "moTaTextBox";
            this.moTaTextBox.Size = new System.Drawing.Size(149, 20);
            this.moTaTextBox.TabIndex = 148;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(269, 313);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 147;
            this.label10.Text = "Tên sách:";
            // 
            // tenSachTextBox
            // 
            this.tenSachTextBox.Enabled = false;
            this.tenSachTextBox.Location = new System.Drawing.Point(330, 310);
            this.tenSachTextBox.Name = "tenSachTextBox";
            this.tenSachTextBox.Size = new System.Drawing.Size(149, 20);
            this.tenSachTextBox.TabIndex = 146;
            // 
            // maNganTuComboBox
            // 
            this.maNganTuComboBox.Enabled = false;
            this.maNganTuComboBox.FormattingEnabled = true;
            this.maNganTuComboBox.Location = new System.Drawing.Point(127, 437);
            this.maNganTuComboBox.Name = "maNganTuComboBox";
            this.maNganTuComboBox.Size = new System.Drawing.Size(121, 21);
            this.maNganTuComboBox.TabIndex = 145;
            this.maNganTuComboBox.SelectedIndexChanged += new System.EventHandler(this.ISBNComboBox_TextChanged);
            this.maNganTuComboBox.TextChanged += new System.EventHandler(this.maNganTuComboBox_TextChanged);
            // 
            // ISBNComboBox
            // 
            this.ISBNComboBox.Enabled = false;
            this.ISBNComboBox.FormattingEnabled = true;
            this.ISBNComboBox.Location = new System.Drawing.Point(124, 310);
            this.ISBNComboBox.Name = "ISBNComboBox";
            this.ISBNComboBox.Size = new System.Drawing.Size(121, 21);
            this.ISBNComboBox.TabIndex = 144;
            this.ISBNComboBox.SelectedIndexChanged += new System.EventHandler(this.ISBNComboBox_TextChanged);
            this.ISBNComboBox.TextChanged += new System.EventHandler(this.ISBNComboBox_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(44, 443);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 143;
            this.label9.Text = "Mã ngăn tủ:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(62, 313);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 142;
            this.label7.Text = "ISBN:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.moveFirstButton);
            this.groupBox1.Controls.Add(this.moveNextButton);
            this.groupBox1.Controls.Add(this.MoveLastButton);
            this.groupBox1.Controls.Add(this.movePreviousButton);
            this.groupBox1.Location = new System.Drawing.Point(59, 194);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(149, 44);
            this.groupBox1.TabIndex = 126;
            this.groupBox1.TabStop = false;
            // 
            // moveFirstButton
            // 
            this.moveFirstButton.Location = new System.Drawing.Point(6, 11);
            this.moveFirstButton.Name = "moveFirstButton";
            this.moveFirstButton.Size = new System.Drawing.Size(30, 23);
            this.moveFirstButton.TabIndex = 11;
            this.moveFirstButton.Text = "<<";
            this.moveFirstButton.UseVisualStyleBackColor = true;
            this.moveFirstButton.Click += new System.EventHandler(this.moveFirstButton_Click);
            // 
            // moveNextButton
            // 
            this.moveNextButton.Location = new System.Drawing.Point(78, 11);
            this.moveNextButton.Name = "moveNextButton";
            this.moveNextButton.Size = new System.Drawing.Size(30, 23);
            this.moveNextButton.TabIndex = 14;
            this.moveNextButton.Text = ">";
            this.moveNextButton.UseVisualStyleBackColor = true;
            this.moveNextButton.Click += new System.EventHandler(this.moveNextButton_Click);
            // 
            // MoveLastButton
            // 
            this.MoveLastButton.Location = new System.Drawing.Point(114, 11);
            this.MoveLastButton.Name = "MoveLastButton";
            this.MoveLastButton.Size = new System.Drawing.Size(30, 23);
            this.MoveLastButton.TabIndex = 12;
            this.MoveLastButton.Text = ">>";
            this.MoveLastButton.UseVisualStyleBackColor = true;
            this.MoveLastButton.Click += new System.EventHandler(this.MoveLastButton_Click);
            // 
            // movePreviousButton
            // 
            this.movePreviousButton.Location = new System.Drawing.Point(42, 11);
            this.movePreviousButton.Name = "movePreviousButton";
            this.movePreviousButton.Size = new System.Drawing.Size(30, 23);
            this.movePreviousButton.TabIndex = 13;
            this.movePreviousButton.Text = "<";
            this.movePreviousButton.UseVisualStyleBackColor = true;
            this.movePreviousButton.Click += new System.EventHandler(this.movePreviousButton_Click);
            // 
            // mySearchButton
            // 
            this.mySearchButton.Location = new System.Drawing.Point(694, 9);
            this.mySearchButton.Name = "mySearchButton";
            this.mySearchButton.Size = new System.Drawing.Size(116, 23);
            this.mySearchButton.TabIndex = 125;
            this.mySearchButton.Text = "Tìm kiếm";
            this.mySearchButton.UseVisualStyleBackColor = true;
            this.mySearchButton.Click += new System.EventHandler(this.mySearchButton_Click);
            // 
            // mySearchTextBox
            // 
            this.mySearchTextBox.Location = new System.Drawing.Point(520, 12);
            this.mySearchTextBox.Name = "mySearchTextBox";
            this.mySearchTextBox.Size = new System.Drawing.Size(168, 20);
            this.mySearchTextBox.TabIndex = 124;
            // 
            // maSachTextBox
            // 
            this.maSachTextBox.Enabled = false;
            this.maSachTextBox.Location = new System.Drawing.Point(127, 345);
            this.maSachTextBox.Name = "maSachTextBox";
            this.maSachTextBox.Size = new System.Drawing.Size(118, 20);
            this.maSachTextBox.TabIndex = 121;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(44, 348);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(51, 13);
            this.label100.TabIndex = 120;
            this.label100.Text = "Mã sách:";
            // 
            // myDataGridView
            // 
            this.myDataGridView.AllowUserToAddRows = false;
            this.myDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.myDataGridView.Location = new System.Drawing.Point(23, 38);
            this.myDataGridView.Name = "myDataGridView";
            this.myDataGridView.ReadOnly = true;
            this.myDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.myDataGridView.Size = new System.Drawing.Size(787, 150);
            this.myDataGridView.TabIndex = 119;
            this.myDataGridView.SelectionChanged += new System.EventHandler(this.myDataGridView_SelectionChanged);
            // 
            // myAddButton
            // 
            this.myAddButton.Location = new System.Drawing.Point(6, 19);
            this.myAddButton.Name = "myAddButton";
            this.myAddButton.Size = new System.Drawing.Size(100, 23);
            this.myAddButton.TabIndex = 6;
            this.myAddButton.Text = "Thêm";
            this.myAddButton.UseVisualStyleBackColor = true;
            this.myAddButton.Click += new System.EventHandler(this.myAddButton_Click);
            // 
            // myCancelButton
            // 
            this.myCancelButton.Enabled = false;
            this.myCancelButton.Location = new System.Drawing.Point(162, 49);
            this.myCancelButton.Name = "myCancelButton";
            this.myCancelButton.Size = new System.Drawing.Size(100, 23);
            this.myCancelButton.TabIndex = 17;
            this.myCancelButton.Text = "Bỏ qua";
            this.myCancelButton.UseVisualStyleBackColor = true;
            this.myCancelButton.Click += new System.EventHandler(this.myCancelButton_Click);
            // 
            // myEditButton
            // 
            this.myEditButton.Location = new System.Drawing.Point(118, 19);
            this.myEditButton.Name = "myEditButton";
            this.myEditButton.Size = new System.Drawing.Size(100, 23);
            this.myEditButton.TabIndex = 8;
            this.myEditButton.Text = "Sửa";
            this.myEditButton.UseVisualStyleBackColor = true;
            this.myEditButton.Click += new System.EventHandler(this.myEditButton_Click);
            // 
            // myDeleteButton
            // 
            this.myDeleteButton.Location = new System.Drawing.Point(230, 19);
            this.myDeleteButton.Name = "myDeleteButton";
            this.myDeleteButton.Size = new System.Drawing.Size(100, 23);
            this.myDeleteButton.TabIndex = 7;
            this.myDeleteButton.Text = "Xóa";
            this.myDeleteButton.UseVisualStyleBackColor = true;
            this.myDeleteButton.Click += new System.EventHandler(this.myDeleteButton_Click);
            // 
            // mySaveButton
            // 
            this.mySaveButton.Enabled = false;
            this.mySaveButton.Location = new System.Drawing.Point(56, 50);
            this.mySaveButton.Name = "mySaveButton";
            this.mySaveButton.Size = new System.Drawing.Size(100, 23);
            this.mySaveButton.TabIndex = 16;
            this.mySaveButton.Text = "Ghi";
            this.mySaveButton.UseVisualStyleBackColor = true;
            this.mySaveButton.Click += new System.EventHandler(this.mySaveButton_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(278, 445);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 149;
            this.label11.Text = "Mô tả:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.myAddButton);
            this.groupBox2.Controls.Add(this.myCancelButton);
            this.groupBox2.Controls.Add(this.mySaveButton);
            this.groupBox2.Controls.Add(this.myEditButton);
            this.groupBox2.Controls.Add(this.myDeleteButton);
            this.groupBox2.Location = new System.Drawing.Point(343, 205);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(340, 83);
            this.groupBox2.TabIndex = 127;
            this.groupBox2.TabStop = false;
            // 
            // tinhTrangComboBox
            // 
            this.tinhTrangComboBox.Enabled = false;
            this.tinhTrangComboBox.FormattingEnabled = true;
            this.tinhTrangComboBox.Items.AddRange(new object[] {
            "Cũ",
            "Mới"});
            this.tinhTrangComboBox.Location = new System.Drawing.Point(127, 377);
            this.tinhTrangComboBox.Name = "tinhTrangComboBox";
            this.tinhTrangComboBox.Size = new System.Drawing.Size(118, 21);
            this.tinhTrangComboBox.TabIndex = 151;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 380);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 150;
            this.label2.Text = "Tình trạng:";
            // 
            // choMuonComboBox
            // 
            this.choMuonComboBox.Enabled = false;
            this.choMuonComboBox.FormattingEnabled = true;
            this.choMuonComboBox.Items.AddRange(new object[] {
            "Chưa Cho Mượn",
            "Đã Cho Mượn"});
            this.choMuonComboBox.Location = new System.Drawing.Point(127, 410);
            this.choMuonComboBox.Name = "choMuonComboBox";
            this.choMuonComboBox.Size = new System.Drawing.Size(118, 21);
            this.choMuonComboBox.TabIndex = 153;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 413);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 152;
            this.label1.Text = "Cho mượn:";
            // 
            // SachForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 470);
            this.Controls.Add(this.choMuonComboBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tinhTrangComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.moTaTextBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tenSachTextBox);
            this.Controls.Add(this.maNganTuComboBox);
            this.Controls.Add(this.ISBNComboBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.mySearchButton);
            this.Controls.Add(this.mySearchTextBox);
            this.Controls.Add(this.maSachTextBox);
            this.Controls.Add(this.label100);
            this.Controls.Add(this.myDataGridView);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.groupBox2);
            this.Name = "SachForm";
            this.Text = "SachForm";
            this.Load += new System.EventHandler(this.myForm_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.myDataGridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox moTaTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tenSachTextBox;
        private System.Windows.Forms.ComboBox maNganTuComboBox;
        private System.Windows.Forms.ComboBox ISBNComboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button moveFirstButton;
        private System.Windows.Forms.Button moveNextButton;
        private System.Windows.Forms.Button MoveLastButton;
        private System.Windows.Forms.Button movePreviousButton;
        private System.Windows.Forms.Button mySearchButton;
        private System.Windows.Forms.TextBox mySearchTextBox;
        private System.Windows.Forms.TextBox maSachTextBox;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.DataGridView myDataGridView;
        private System.Windows.Forms.Button myAddButton;
        private System.Windows.Forms.Button myCancelButton;
        private System.Windows.Forms.Button myEditButton;
        private System.Windows.Forms.Button myDeleteButton;
        private System.Windows.Forms.Button mySaveButton;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox tinhTrangComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox choMuonComboBox;
        private System.Windows.Forms.Label label1;
    }
}