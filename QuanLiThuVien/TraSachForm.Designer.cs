﻿namespace QuanLiThuVien
{
    partial class TraSachForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.myDataGridView = new System.Windows.Forms.DataGridView();
            this.maSachComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.maNhanVienTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ngayTraDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ngayMuonDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.soNgayQuanHanTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TienPhatTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.tinhTrangTraComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.maPhieuComboBox = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.XemThongTinButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tinhTrangMuonComboBox = new System.Windows.Forms.ComboBox();
            this.maDocGiaTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chiuPhatLabel = new System.Windows.Forms.Label();
            this.hongSachTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.myDataGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // myDataGridView
            // 
            this.myDataGridView.AllowUserToAddRows = false;
            this.myDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.myDataGridView.Location = new System.Drawing.Point(26, 38);
            this.myDataGridView.Name = "myDataGridView";
            this.myDataGridView.ReadOnly = true;
            this.myDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.myDataGridView.Size = new System.Drawing.Size(787, 150);
            this.myDataGridView.TabIndex = 90;
            this.myDataGridView.SelectionChanged += new System.EventHandler(this.myDataGridView_SelectionChanged);
            // 
            // maSachComboBox
            // 
            this.maSachComboBox.FormattingEnabled = true;
            this.maSachComboBox.Location = new System.Drawing.Point(117, 245);
            this.maSachComboBox.Name = "maSachComboBox";
            this.maSachComboBox.Size = new System.Drawing.Size(121, 21);
            this.maSachComboBox.TabIndex = 94;
            this.maSachComboBox.TextChanged += new System.EventHandler(this.maSachComboBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 248);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 93;
            this.label2.Text = "Mã sách:";
            // 
            // maNhanVienTextBox
            // 
            this.maNhanVienTextBox.Enabled = false;
            this.maNhanVienTextBox.Location = new System.Drawing.Point(117, 277);
            this.maNhanVienTextBox.Name = "maNhanVienTextBox";
            this.maNhanVienTextBox.Size = new System.Drawing.Size(121, 20);
            this.maNhanVienTextBox.TabIndex = 96;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 280);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 95;
            this.label4.Text = "Mã nhân viên:";
            // 
            // ngayTraDateTimePicker
            // 
            this.ngayTraDateTimePicker.Enabled = false;
            this.ngayTraDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ngayTraDateTimePicker.Location = new System.Drawing.Point(118, 308);
            this.ngayTraDateTimePicker.Name = "ngayTraDateTimePicker";
            this.ngayTraDateTimePicker.Size = new System.Drawing.Size(121, 20);
            this.ngayTraDateTimePicker.TabIndex = 98;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(36, 311);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 97;
            this.label7.Text = "Ngày trả:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 13);
            this.label3.TabIndex = 99;
            this.label3.Text = "Tình trạng sách khi mượn:";
            // 
            // ngayMuonDateTimePicker
            // 
            this.ngayMuonDateTimePicker.Enabled = false;
            this.ngayMuonDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ngayMuonDateTimePicker.Location = new System.Drawing.Point(152, 54);
            this.ngayMuonDateTimePicker.Name = "ngayMuonDateTimePicker";
            this.ngayMuonDateTimePicker.Size = new System.Drawing.Size(121, 20);
            this.ngayMuonDateTimePicker.TabIndex = 102;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 101;
            this.label5.Text = "Ngày mượn:";
            // 
            // soNgayQuanHanTextBox
            // 
            this.soNgayQuanHanTextBox.Enabled = false;
            this.soNgayQuanHanTextBox.Location = new System.Drawing.Point(388, 51);
            this.soNgayQuanHanTextBox.Name = "soNgayQuanHanTextBox";
            this.soNgayQuanHanTextBox.Size = new System.Drawing.Size(106, 20);
            this.soNgayQuanHanTextBox.TabIndex = 104;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(291, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 13);
            this.label6.TabIndex = 103;
            this.label6.Text = "Số ngày quá hạn:";
            // 
            // TienPhatTextBox
            // 
            this.TienPhatTextBox.Enabled = false;
            this.TienPhatTextBox.Location = new System.Drawing.Point(388, 86);
            this.TienPhatTextBox.Name = "TienPhatTextBox";
            this.TienPhatTextBox.Size = new System.Drawing.Size(106, 20);
            this.TienPhatTextBox.TabIndex = 106;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(291, 88);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 105;
            this.label8.Text = "Tiền phạt:";
            // 
            // saveButton
            // 
            this.saveButton.Enabled = false;
            this.saveButton.Location = new System.Drawing.Point(298, 129);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(207, 27);
            this.saveButton.TabIndex = 107;
            this.saveButton.Text = "Trả sách";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // tinhTrangTraComboBox
            // 
            this.tinhTrangTraComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tinhTrangTraComboBox.Enabled = false;
            this.tinhTrangTraComboBox.FormattingEnabled = true;
            this.tinhTrangTraComboBox.Items.AddRange(new object[] {
            "Cũ",
            "Mới"});
            this.tinhTrangTraComboBox.Location = new System.Drawing.Point(152, 117);
            this.tinhTrangTraComboBox.Name = "tinhTrangTraComboBox";
            this.tinhTrangTraComboBox.Size = new System.Drawing.Size(121, 21);
            this.tinhTrangTraComboBox.TabIndex = 109;
            this.tinhTrangTraComboBox.TextChanged += new System.EventHandler(this.tinhTrangTraComboBox_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 117);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(116, 13);
            this.label9.TabIndex = 108;
            this.label9.Text = "Tình trạng sách lúc trả:";
            // 
            // maPhieuComboBox
            // 
            this.maPhieuComboBox.FormattingEnabled = true;
            this.maPhieuComboBox.Location = new System.Drawing.Point(117, 210);
            this.maPhieuComboBox.Name = "maPhieuComboBox";
            this.maPhieuComboBox.Size = new System.Drawing.Size(121, 21);
            this.maPhieuComboBox.TabIndex = 111;
            this.maPhieuComboBox.TextChanged += new System.EventHandler(this.maPhieuComboBox_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(35, 213);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 110;
            this.label10.Text = "Mã phiếu:";
            // 
            // XemThongTinButton
            // 
            this.XemThongTinButton.Location = new System.Drawing.Point(17, 19);
            this.XemThongTinButton.Name = "XemThongTinButton";
            this.XemThongTinButton.Size = new System.Drawing.Size(488, 27);
            this.XemThongTinButton.TabIndex = 112;
            this.XemThongTinButton.Text = "Xem thông tin";
            this.XemThongTinButton.UseVisualStyleBackColor = true;
            this.XemThongTinButton.Click += new System.EventHandler(this.XemThongTinButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.hongSachTextBox);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.chiuPhatLabel);
            this.groupBox1.Controls.Add(this.tinhTrangMuonComboBox);
            this.groupBox1.Controls.Add(this.XemThongTinButton);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.ngayMuonDateTimePicker);
            this.groupBox1.Controls.Add(this.tinhTrangTraComboBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.saveButton);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.TienPhatTextBox);
            this.groupBox1.Controls.Add(this.soNgayQuanHanTextBox);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(281, 194);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(532, 182);
            this.groupBox1.TabIndex = 113;
            this.groupBox1.TabStop = false;
            // 
            // tinhTrangMuonComboBox
            // 
            this.tinhTrangMuonComboBox.Enabled = false;
            this.tinhTrangMuonComboBox.FormattingEnabled = true;
            this.tinhTrangMuonComboBox.Items.AddRange(new object[] {
            "Cũ",
            "Mới"});
            this.tinhTrangMuonComboBox.Location = new System.Drawing.Point(152, 90);
            this.tinhTrangMuonComboBox.Name = "tinhTrangMuonComboBox";
            this.tinhTrangMuonComboBox.Size = new System.Drawing.Size(121, 21);
            this.tinhTrangMuonComboBox.TabIndex = 114;
            this.tinhTrangMuonComboBox.TextChanged += new System.EventHandler(this.tinhTrangMuonComboBox_TextChanged);
            // 
            // maDocGiaTextBox
            // 
            this.maDocGiaTextBox.Location = new System.Drawing.Point(653, 10);
            this.maDocGiaTextBox.Name = "maDocGiaTextBox";
            this.maDocGiaTextBox.Size = new System.Drawing.Size(160, 20);
            this.maDocGiaTextBox.TabIndex = 115;
            this.maDocGiaTextBox.TextChanged += new System.EventHandler(this.maDocGiaTextBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(538, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 114;
            this.label1.Text = "Lọc theo Mã độc giả:";
            // 
            // chiuPhatLabel
            // 
            this.chiuPhatLabel.AutoSize = true;
            this.chiuPhatLabel.Location = new System.Drawing.Point(30, 146);
            this.chiuPhatLabel.Name = "chiuPhatLabel";
            this.chiuPhatLabel.Size = new System.Drawing.Size(0, 13);
            this.chiuPhatLabel.TabIndex = 116;
            // 
            // hongSachTextBox
            // 
            this.hongSachTextBox.Enabled = false;
            this.hongSachTextBox.Location = new System.Drawing.Point(152, 144);
            this.hongSachTextBox.Name = "hongSachTextBox";
            this.hongSachTextBox.Size = new System.Drawing.Size(119, 20);
            this.hongSachTextBox.TabIndex = 118;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 147);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(134, 13);
            this.label11.TabIndex = 117;
            this.label11.Text = "Sử phạt do làm hỏng sách:";
            // 
            // TraSachForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 409);
            this.Controls.Add(this.maDocGiaTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.maPhieuComboBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.ngayTraDateTimePicker);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.maNhanVienTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.maSachComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.myDataGridView);
            this.Name = "TraSachForm";
            this.Text = "TraSachForm";
            this.Load += new System.EventHandler(this.TraSachForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.myDataGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView myDataGridView;
        private System.Windows.Forms.ComboBox maSachComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox maNhanVienTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker ngayTraDateTimePicker;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker ngayMuonDateTimePicker;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox soNgayQuanHanTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TienPhatTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.ComboBox tinhTrangTraComboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox maPhieuComboBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button XemThongTinButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox tinhTrangMuonComboBox;
        private System.Windows.Forms.TextBox maDocGiaTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox hongSachTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label chiuPhatLabel;
    }
}