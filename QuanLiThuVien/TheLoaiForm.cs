﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLiThuVien
{
    public partial class TheLoaiForm : Form
    {
        public TheLoaiForm()
        {
            InitializeComponent();
        }

        private void TheLoaiForm_Load(object sender, EventArgs e)
        {
            // Resize the DataGridView columns to fit the newly loaded content.
            getDataList();

        }
        private const String SPSelect = "THELOAI_XEM";
        private const String SPInsert = "THELOAI_THEM";
        private const String SPUpdate = "THELOAI_SUA";
        private const String SPDelete = "THELOAI_XOA";
        private const Boolean isAutoIncreasePrimaryKey = false;
        private String oldPrimaryKey = ""; 
        private void getDataList()
        {
            myDataGridView = GlobalFunction.getDataList(myDataGridView, SPSelect);  
                        
        }
        private void theLoaiDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private String action = ""; 
        private void themTheLoaibutton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.AddNew();
            action = "Add"; 
            enableActionForAddAndEditButton();    
        }
        private void SuaTheLoaiButton_Click(object sender, EventArgs e)
        {
            action = "Edit";
            oldPrimaryKey = maTheLoaiTextBox.Text; 
            enableActionForAddAndEditButton();
        }

        private void xoaTheLoaiButton_Click(object sender, EventArgs e)
        {

            String primaryKey = maTheLoaiTextBox.Text;
            GlobalFunction.deleteRow(SPDelete, primaryKey, "Mã thể loại");
            getDataList(); 
            
        }

        private void theLoaiDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in myDataGridView.SelectedRows)
            {
                maTheLoaiTextBox.Text = row.Cells[0].Value.ToString();
                tenTheLoaiTextBox.Text = row.Cells[1].Value.ToString();
               
            }
   
        }

        

     
        private void searchTheLoaiButton_Click(object sender, EventArgs e)
        {
         
            GlobalFunction.searchData(searchTheLoaiTextBox.Text, myDataGridView); 
                 
        }

        private void moveFirstButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveFirst(); 
        }

        private void MoveLastButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveLast();
        }

     

        private void movePreviousButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MovePrevious(); 
        }

        private void moveNextButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveNext(); 
        }

        private void ghiTheLoaiButton_Click(object sender, EventArgs e)
        {
            String primaryKey = maTheLoaiTextBox.Text;
            String theLoaiName = tenTheLoaiTextBox.Text;

            List<String> otherStringValues = new List<String>();
            otherStringValues.Add(primaryKey);
            otherStringValues.Add(theLoaiName);

            List<String> otherStringValuesLabel = new List<String>();
            otherStringValuesLabel.Add("Mã thể loại");
            otherStringValuesLabel.Add("Tên thể loại");

            List<int> primaryKeyIndex = new List<int>();
            primaryKeyIndex.Add(0);

           // String sqlInsert = "EXEC " + SPInsert + " '" + primaryKey+"', '" +theLoaiName + "'";
            //String sqlUpdate = "EXEC " + SPUpdate + " '" + oldPrimaryKey+"', '"+ primaryKey + "' ,'" + theLoaiName + "'";
            GlobalFunction.oldPrimaryKey = oldPrimaryKey; 
            myDataGridView = GlobalFunction.saveActionAuto(myDataGridView, isAutoIncreasePrimaryKey, primaryKeyIndex, otherStringValues, otherStringValuesLabel, action, SPInsert, SPUpdate, SPSelect);

            enableActionForSaveAndCancelButton();
           
        }

        private void phucHoiTheLoaiButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.CancelEdit();
 
            enableActionForSaveAndCancelButton();
        }
        private void enableActionForSaveAndCancelButton()
        {
            BoQuaTheLoaiButton.Enabled = false;
            ghiTheLoaiButton.Enabled = false;
            themTheLoaibutton.Enabled = true;
            maTheLoaiTextBox.Enabled = false;
            tenTheLoaiTextBox.Enabled = false;
            SuaTheLoaiButton.Enabled = true;
            xoaTheLoaiButton.Enabled = true;
            myDataGridView.Enabled = true; 
        }
        private void enableActionForAddAndEditButton()
        {
            ghiTheLoaiButton.Enabled = true;   
            maTheLoaiTextBox.Enabled = true;
            tenTheLoaiTextBox.Enabled = true;
            BoQuaTheLoaiButton.Enabled = true;
            myDataGridView.Enabled = false;
            xoaTheLoaiButton.Enabled = false;
            themTheLoaibutton.Enabled = false;
            SuaTheLoaiButton.Enabled = false; 
            maTheLoaiTextBox.Focus();

        }

       
        
    }
}
