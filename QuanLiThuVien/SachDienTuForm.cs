﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLiThuVien
{
    public partial class SachDienTuForm : Form
    {
        public SachDienTuForm()
        {
            InitializeComponent();
        }
        private void myForm_Load(object sender, EventArgs e)
        {
            getDataList();
        }
        // TODO: change
        private const String SPSelect = "SACH_DIENTU_XEM";
        private const String SPInsert = "SACH_DIENTU_THEM";
        private const String SPUpdate = "SACH_DIENTU_SUA";
        private const String SPDelete = "SACH_DIENTU_XOA";
        private const Boolean isAutoIncreasePrimaryKey = false;

        private BindingSource ISBNBindingSource;
        
        // ---------------------------------
        private void getDataList()
        {
            myDataGridView = GlobalFunction.getDataList(myDataGridView, SPSelect);

          
            // get data for ISNB ComboBox
            ISBNBindingSource = Program.getBindingSource("exec ISBN_XEM");

            for (int i = 0; i < ISBNBindingSource.List.Count; i++)
            {
                DataRowView current = (DataRowView)ISBNBindingSource.List[i];
                ISBNComboBox.Items.Add(current["ISBN"].ToString());

            }
        }




        private String action = "";
        private void myAddButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.AddNew();
            action = "Add";
            enableActionForAddAndEditButton();
        }
        private void myEditButton_Click(object sender, EventArgs e)
        {
            action = "Edit";
            enableActionForAddAndEditButton();
            GlobalFunction.oldPrimaryKey = maSachTextBox.Text; 
        }

        private void myDeleteButton_Click(object sender, EventArgs e)
        {
            // TODO: change
            String primaryKey = maSachTextBox.Text;
            GlobalFunction.deleteRow(SPDelete, primaryKey, "Mã sách");
            // -----------------------------
            getDataList();

        }
        private void myDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in myDataGridView.SelectedRows)
            {
                // TODO: change
                try
                {
                    maSachTextBox.Text = row.Cells[0].Value.ToString();
                    filePathTextBox.Text = row.Cells[1].Value.ToString();
                    soLanDownloadTextBox.Text = row.Cells[2].Value.ToString();                 
                    choPhepDownloadComboBox.SelectedIndex = (row.Cells[3].Value.ToString().Equals("False") ? 0 : 1);
                    ISBNComboBox.SelectedItem = row.Cells[4].Value.ToString(); 
                    
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message); 
                }
            



                //------------------------------------------------------------

            }


        }

        private void mySaveButton_Click(object sender, EventArgs e)
        {
           
            // TODO: kiểm tra kiểu dữ liệu 
            int i; 
            if (!Int32.TryParse(maSachTextBox.Text , out i ))
            {
                MessageBox.Show("Mã sách phải là số nguyên.", "Lỗi", MessageBoxButtons.OK);
                return; 
            }
            if (!Int32.TryParse(soLanDownloadTextBox.Text, out i))
            {
                MessageBox.Show("Số lần download phải là số nguyên.", "Lỗi", MessageBoxButtons.OK);
                return;
            }

             //////////////////////////////////////


            String maSach = maSachTextBox.Text;
            String filePath = filePathTextBox.Text;
            String soLanDownload = soLanDownloadTextBox.Text;
            String choPhepDownload = choPhepDownloadComboBox.SelectedIndex.ToString();
            String ISBN = ISBNComboBox.SelectedItem.ToString(); 

            List<String> textBoxValues = new List<String>();
            textBoxValues.Add(maSach);
            textBoxValues.Add(filePath);
            textBoxValues.Add(soLanDownload);
            textBoxValues.Add(choPhepDownload);
            textBoxValues.Add(ISBN);
         

            List<String> lableForTextBoxValues = new List<String>();
            lableForTextBoxValues.Add("Mã sách");
            lableForTextBoxValues.Add("File Path");
            lableForTextBoxValues.Add("Số lần download");
            lableForTextBoxValues.Add("Cho phép download");
            lableForTextBoxValues.Add("ISBN");
          
            List<int> primaryKeyIndex = new List<int>();
            primaryKeyIndex.Add(0);

        
            myDataGridView = GlobalFunction.saveActionAuto(myDataGridView, isAutoIncreasePrimaryKey, primaryKeyIndex, textBoxValues, lableForTextBoxValues, action, SPInsert, SPUpdate, SPSelect);
            enableActionForSaveAndCancelButton();

        }

        private void myCancelButton_Click(object sender, EventArgs e)
        {
            GlobalFunction.cancelAciton();
         

            enableActionForSaveAndCancelButton();
        }

        private void mySearchButton_Click(object sender, EventArgs e)
        {
            GlobalFunction.searchData(mySearchTextBox.Text, myDataGridView);
        }

        private void moveFirstButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveFirst();
        }

        private void MoveLastButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveLast();
        }

        private void movePreviousButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MovePrevious();
        }

        private void moveNextButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveNext();
        }


        private void enableActionForSaveAndCancelButton()
        {
            // TODO: change
            // diable textbox
            maSachTextBox.Enabled = false;
            filePathTextBox.Enabled = false;
            soLanDownloadTextBox.Enabled = false;    
            choPhepDownloadComboBox.Enabled = false;
            ISBNComboBox.Enabled = false; 
           
            ///////////////////////////////////////
            // config button
            myCancelButton.Enabled = false;
            mySaveButton.Enabled = false;
            myAddButton.Enabled = true;
            myEditButton.Enabled = true;
            myDeleteButton.Enabled = true;
            myDataGridView.Enabled = true;
        }
        private void enableActionForAddAndEditButton()
        {
            // TODO: change     
            //config textbox
            if (!isAutoIncreasePrimaryKey)
            {
                maSachTextBox.Enabled = true;
            }
            else
            {
                maSachTextBox.Enabled = false;
            }

            filePathTextBox.Enabled = true;
            soLanDownloadTextBox.Enabled = true;
            choPhepDownloadComboBox.Enabled = true;
            ISBNComboBox.Enabled = true; 
            //////////////////////////////////////////
            // config button
            mySaveButton.Enabled = true;
            myCancelButton.Enabled = true;
            myDataGridView.Enabled = false;
            myDeleteButton.Enabled = false;
            myAddButton.Enabled = false;
            myEditButton.Enabled = false;
            maSachTextBox.Focus();

        }

       
    
    }
}
