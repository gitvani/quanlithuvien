﻿namespace QuanLiThuVien
{
    partial class ISBNForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.myCancelButton = new System.Windows.Forms.Button();
            this.myEditButton = new System.Windows.Forms.Button();
            this.myDeleteButton = new System.Windows.Forms.Button();
            this.moveFirstButton = new System.Windows.Forms.Button();
            this.moveNextButton = new System.Windows.Forms.Button();
            this.MoveLastButton = new System.Windows.Forms.Button();
            this.movePreviousButton = new System.Windows.Forms.Button();
            this.mySaveButton = new System.Windows.Forms.Button();
            this.ngayXuatBanTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.noiDungTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.soTrangTextBox = new System.Windows.Forms.TextBox();
            this.khoSachTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lanXuatBanTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.myAddButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.mySearchButton = new System.Windows.Forms.Button();
            this.mySearchTextBox = new System.Windows.Forms.TextBox();
            this.tenSachTextBox = new System.Windows.Forms.TextBox();
            this.ISBNTextBox = new System.Windows.Forms.TextBox();
            this.maNgonNguLabel = new System.Windows.Forms.Label();
            this.myDataGridView = new System.Windows.Forms.DataGridView();
            this.tenNgonNguLabel = new System.Windows.Forms.Label();
            this.hinhAnhPathTextBox = new System.Windows.Forms.TextBox();
            this.giaTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.maNgonNguComboBox = new System.Windows.Forms.ComboBox();
            this.maTheLoaiComboBox = new System.Windows.Forms.ComboBox();
            this.tenNgonNguTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tenTheLoaiTextBox = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.myDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // myCancelButton
            // 
            this.myCancelButton.Enabled = false;
            this.myCancelButton.Location = new System.Drawing.Point(162, 49);
            this.myCancelButton.Name = "myCancelButton";
            this.myCancelButton.Size = new System.Drawing.Size(100, 23);
            this.myCancelButton.TabIndex = 17;
            this.myCancelButton.Text = "Bỏ qua";
            this.myCancelButton.UseVisualStyleBackColor = true;
            this.myCancelButton.Click += new System.EventHandler(this.myCancelButton_Click);
            // 
            // myEditButton
            // 
            this.myEditButton.Location = new System.Drawing.Point(118, 19);
            this.myEditButton.Name = "myEditButton";
            this.myEditButton.Size = new System.Drawing.Size(100, 23);
            this.myEditButton.TabIndex = 8;
            this.myEditButton.Text = "Sửa";
            this.myEditButton.UseVisualStyleBackColor = true;
            this.myEditButton.Click += new System.EventHandler(this.myEditButton_Click);
            // 
            // myDeleteButton
            // 
            this.myDeleteButton.Location = new System.Drawing.Point(230, 19);
            this.myDeleteButton.Name = "myDeleteButton";
            this.myDeleteButton.Size = new System.Drawing.Size(100, 23);
            this.myDeleteButton.TabIndex = 7;
            this.myDeleteButton.Text = "Xóa";
            this.myDeleteButton.UseVisualStyleBackColor = true;
            this.myDeleteButton.Click += new System.EventHandler(this.myDeleteButton_Click);
            // 
            // moveFirstButton
            // 
            this.moveFirstButton.Location = new System.Drawing.Point(6, 11);
            this.moveFirstButton.Name = "moveFirstButton";
            this.moveFirstButton.Size = new System.Drawing.Size(30, 23);
            this.moveFirstButton.TabIndex = 11;
            this.moveFirstButton.Text = "<<";
            this.moveFirstButton.UseVisualStyleBackColor = true;
            this.moveFirstButton.Click += new System.EventHandler(this.moveFirstButton_Click);
            // 
            // moveNextButton
            // 
            this.moveNextButton.Location = new System.Drawing.Point(78, 11);
            this.moveNextButton.Name = "moveNextButton";
            this.moveNextButton.Size = new System.Drawing.Size(30, 23);
            this.moveNextButton.TabIndex = 14;
            this.moveNextButton.Text = ">";
            this.moveNextButton.UseVisualStyleBackColor = true;
            this.moveNextButton.Click += new System.EventHandler(this.moveNextButton_Click);
            // 
            // MoveLastButton
            // 
            this.MoveLastButton.Location = new System.Drawing.Point(114, 11);
            this.MoveLastButton.Name = "MoveLastButton";
            this.MoveLastButton.Size = new System.Drawing.Size(30, 23);
            this.MoveLastButton.TabIndex = 12;
            this.MoveLastButton.Text = ">>";
            this.MoveLastButton.UseVisualStyleBackColor = true;
            this.MoveLastButton.Click += new System.EventHandler(this.MoveLastButton_Click);
            // 
            // movePreviousButton
            // 
            this.movePreviousButton.Location = new System.Drawing.Point(42, 11);
            this.movePreviousButton.Name = "movePreviousButton";
            this.movePreviousButton.Size = new System.Drawing.Size(30, 23);
            this.movePreviousButton.TabIndex = 13;
            this.movePreviousButton.Text = "<";
            this.movePreviousButton.UseVisualStyleBackColor = true;
            this.movePreviousButton.Click += new System.EventHandler(this.movePreviousButton_Click);
            // 
            // mySaveButton
            // 
            this.mySaveButton.Enabled = false;
            this.mySaveButton.Location = new System.Drawing.Point(56, 50);
            this.mySaveButton.Name = "mySaveButton";
            this.mySaveButton.Size = new System.Drawing.Size(100, 23);
            this.mySaveButton.TabIndex = 16;
            this.mySaveButton.Text = "Ghi";
            this.mySaveButton.UseVisualStyleBackColor = true;
            this.mySaveButton.Click += new System.EventHandler(this.mySaveButton_Click);
            // 
            // ngayXuatBanTimePicker
            // 
            this.ngayXuatBanTimePicker.Enabled = false;
            this.ngayXuatBanTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ngayXuatBanTimePicker.Location = new System.Drawing.Point(126, 445);
            this.ngayXuatBanTimePicker.Name = "ngayXuatBanTimePicker";
            this.ngayXuatBanTimePicker.Size = new System.Drawing.Size(149, 20);
            this.ngayXuatBanTimePicker.TabIndex = 101;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(41, 451);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 100;
            this.label8.Text = "Ngày xuất bản:";
            // 
            // noiDungTextBox
            // 
            this.noiDungTextBox.Enabled = false;
            this.noiDungTextBox.Location = new System.Drawing.Point(126, 374);
            this.noiDungTextBox.Name = "noiDungTextBox";
            this.noiDungTextBox.Size = new System.Drawing.Size(149, 20);
            this.noiDungTextBox.TabIndex = 97;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(43, 377);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 96;
            this.label6.Text = "Nội dung:";
            // 
            // soTrangTextBox
            // 
            this.soTrangTextBox.Enabled = false;
            this.soTrangTextBox.Location = new System.Drawing.Point(408, 358);
            this.soTrangTextBox.Name = "soTrangTextBox";
            this.soTrangTextBox.Size = new System.Drawing.Size(149, 20);
            this.soTrangTextBox.TabIndex = 94;
            // 
            // khoSachTextBox
            // 
            this.khoSachTextBox.Enabled = false;
            this.khoSachTextBox.Location = new System.Drawing.Point(126, 344);
            this.khoSachTextBox.Name = "khoSachTextBox";
            this.khoSachTextBox.Size = new System.Drawing.Size(149, 20);
            this.khoSachTextBox.TabIndex = 92;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 347);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 91;
            this.label4.Text = "Kho sách:";
            // 
            // lanXuatBanTextBox
            // 
            this.lanXuatBanTextBox.Enabled = false;
            this.lanXuatBanTextBox.Location = new System.Drawing.Point(126, 479);
            this.lanXuatBanTextBox.Name = "lanXuatBanTextBox";
            this.lanXuatBanTextBox.Size = new System.Drawing.Size(149, 20);
            this.lanXuatBanTextBox.TabIndex = 90;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 482);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 89;
            this.label3.Text = "Lần xuất bản:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 411);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 88;
            this.label2.Text = "Hình ảnh path:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.myAddButton);
            this.groupBox2.Controls.Add(this.myCancelButton);
            this.groupBox2.Controls.Add(this.mySaveButton);
            this.groupBox2.Controls.Add(this.myEditButton);
            this.groupBox2.Controls.Add(this.myDeleteButton);
            this.groupBox2.Location = new System.Drawing.Point(342, 225);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(340, 83);
            this.groupBox2.TabIndex = 85;
            this.groupBox2.TabStop = false;
            // 
            // myAddButton
            // 
            this.myAddButton.Location = new System.Drawing.Point(6, 19);
            this.myAddButton.Name = "myAddButton";
            this.myAddButton.Size = new System.Drawing.Size(100, 23);
            this.myAddButton.TabIndex = 6;
            this.myAddButton.Text = "Thêm";
            this.myAddButton.UseVisualStyleBackColor = true;
            this.myAddButton.Click += new System.EventHandler(this.myAddButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.moveFirstButton);
            this.groupBox1.Controls.Add(this.moveNextButton);
            this.groupBox1.Controls.Add(this.MoveLastButton);
            this.groupBox1.Controls.Add(this.movePreviousButton);
            this.groupBox1.Location = new System.Drawing.Point(58, 214);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(149, 44);
            this.groupBox1.TabIndex = 84;
            this.groupBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(330, 362);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 93;
            this.label5.Text = "Số trang:";
            // 
            // mySearchButton
            // 
            this.mySearchButton.Location = new System.Drawing.Point(693, 29);
            this.mySearchButton.Name = "mySearchButton";
            this.mySearchButton.Size = new System.Drawing.Size(116, 23);
            this.mySearchButton.TabIndex = 83;
            this.mySearchButton.Text = "Tìm kiếm";
            this.mySearchButton.UseVisualStyleBackColor = true;
            this.mySearchButton.Click += new System.EventHandler(this.mySearchButton_Click);
            // 
            // mySearchTextBox
            // 
            this.mySearchTextBox.Location = new System.Drawing.Point(519, 32);
            this.mySearchTextBox.Name = "mySearchTextBox";
            this.mySearchTextBox.Size = new System.Drawing.Size(168, 20);
            this.mySearchTextBox.TabIndex = 82;
            // 
            // tenSachTextBox
            // 
            this.tenSachTextBox.Enabled = false;
            this.tenSachTextBox.Location = new System.Drawing.Point(126, 312);
            this.tenSachTextBox.Name = "tenSachTextBox";
            this.tenSachTextBox.Size = new System.Drawing.Size(149, 20);
            this.tenSachTextBox.TabIndex = 81;
            // 
            // ISBNTextBox
            // 
            this.ISBNTextBox.Enabled = false;
            this.ISBNTextBox.Location = new System.Drawing.Point(126, 277);
            this.ISBNTextBox.Name = "ISBNTextBox";
            this.ISBNTextBox.Size = new System.Drawing.Size(149, 20);
            this.ISBNTextBox.TabIndex = 79;
            // 
            // maNgonNguLabel
            // 
            this.maNgonNguLabel.AutoSize = true;
            this.maNgonNguLabel.Location = new System.Drawing.Point(43, 280);
            this.maNgonNguLabel.Name = "maNgonNguLabel";
            this.maNgonNguLabel.Size = new System.Drawing.Size(35, 13);
            this.maNgonNguLabel.TabIndex = 78;
            this.maNgonNguLabel.Text = "ISBN:";
            // 
            // myDataGridView
            // 
            this.myDataGridView.AllowUserToAddRows = false;
            this.myDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.myDataGridView.Location = new System.Drawing.Point(22, 58);
            this.myDataGridView.Name = "myDataGridView";
            this.myDataGridView.ReadOnly = true;
            this.myDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.myDataGridView.Size = new System.Drawing.Size(787, 150);
            this.myDataGridView.TabIndex = 77;
            this.myDataGridView.SelectionChanged += new System.EventHandler(this.myDataGridView_SelectionChanged);
            // 
            // tenNgonNguLabel
            // 
            this.tenNgonNguLabel.AutoSize = true;
            this.tenNgonNguLabel.Location = new System.Drawing.Point(39, 312);
            this.tenNgonNguLabel.Name = "tenNgonNguLabel";
            this.tenNgonNguLabel.Size = new System.Drawing.Size(55, 13);
            this.tenNgonNguLabel.TabIndex = 80;
            this.tenNgonNguLabel.Text = "Tên sách:";
            // 
            // hinhAnhPathTextBox
            // 
            this.hinhAnhPathTextBox.Enabled = false;
            this.hinhAnhPathTextBox.Location = new System.Drawing.Point(126, 408);
            this.hinhAnhPathTextBox.Name = "hinhAnhPathTextBox";
            this.hinhAnhPathTextBox.Size = new System.Drawing.Size(149, 20);
            this.hinhAnhPathTextBox.TabIndex = 106;
            // 
            // giaTextBox
            // 
            this.giaTextBox.Enabled = false;
            this.giaTextBox.Location = new System.Drawing.Point(408, 388);
            this.giaTextBox.Name = "giaTextBox";
            this.giaTextBox.Size = new System.Drawing.Size(149, 20);
            this.giaTextBox.TabIndex = 108;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(330, 392);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 107;
            this.label1.Text = "Giá:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(330, 421);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 109;
            this.label7.Text = "Mã ngôn ngữ:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(330, 450);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 111;
            this.label9.Text = "Mã thể loại:";
            // 
            // maNgonNguComboBox
            // 
            this.maNgonNguComboBox.Enabled = false;
            this.maNgonNguComboBox.FormattingEnabled = true;
            this.maNgonNguComboBox.Location = new System.Drawing.Point(409, 421);
            this.maNgonNguComboBox.Name = "maNgonNguComboBox";
            this.maNgonNguComboBox.Size = new System.Drawing.Size(121, 21);
            this.maNgonNguComboBox.TabIndex = 113;
            this.maNgonNguComboBox.SelectedIndexChanged += new System.EventHandler(this.maNgonNguComboBox_SelectedIndexChanged);
            // 
            // maTheLoaiComboBox
            // 
            this.maTheLoaiComboBox.Enabled = false;
            this.maTheLoaiComboBox.FormattingEnabled = true;
            this.maTheLoaiComboBox.Location = new System.Drawing.Point(408, 451);
            this.maTheLoaiComboBox.Name = "maTheLoaiComboBox";
            this.maTheLoaiComboBox.Size = new System.Drawing.Size(121, 21);
            this.maTheLoaiComboBox.TabIndex = 114;
            this.maTheLoaiComboBox.SelectedIndexChanged += new System.EventHandler(this.maTheLoaiComboBox_SelectedIndexChanged);
            // 
            // tenNgonNguTextBox
            // 
            this.tenNgonNguTextBox.Enabled = false;
            this.tenNgonNguTextBox.Location = new System.Drawing.Point(621, 422);
            this.tenNgonNguTextBox.Name = "tenNgonNguTextBox";
            this.tenNgonNguTextBox.Size = new System.Drawing.Size(149, 20);
            this.tenNgonNguTextBox.TabIndex = 115;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(542, 425);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 13);
            this.label10.TabIndex = 116;
            this.label10.Text = "Tên ngôn ngữ:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(542, 459);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 13);
            this.label11.TabIndex = 118;
            this.label11.Text = "Tên thể loại:";
            // 
            // tenTheLoaiTextBox
            // 
            this.tenTheLoaiTextBox.Enabled = false;
            this.tenTheLoaiTextBox.Location = new System.Drawing.Point(621, 456);
            this.tenTheLoaiTextBox.Name = "tenTheLoaiTextBox";
            this.tenTheLoaiTextBox.Size = new System.Drawing.Size(149, 20);
            this.tenTheLoaiTextBox.TabIndex = 117;
            // 
            // ISBNForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 543);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tenTheLoaiTextBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tenNgonNguTextBox);
            this.Controls.Add(this.maTheLoaiComboBox);
            this.Controls.Add(this.maNgonNguComboBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.giaTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.hinhAnhPathTextBox);
            this.Controls.Add(this.ngayXuatBanTimePicker);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.noiDungTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.soTrangTextBox);
            this.Controls.Add(this.khoSachTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lanXuatBanTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.mySearchButton);
            this.Controls.Add(this.mySearchTextBox);
            this.Controls.Add(this.tenSachTextBox);
            this.Controls.Add(this.ISBNTextBox);
            this.Controls.Add(this.maNgonNguLabel);
            this.Controls.Add(this.myDataGridView);
            this.Controls.Add(this.tenNgonNguLabel);
            this.Name = "ISBNForm";
            this.Text = "ISBN";
            this.Load += new System.EventHandler(this.myForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.myDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button myCancelButton;
        private System.Windows.Forms.Button myEditButton;
        private System.Windows.Forms.Button myDeleteButton;
        private System.Windows.Forms.Button moveFirstButton;
        private System.Windows.Forms.Button moveNextButton;
        private System.Windows.Forms.Button MoveLastButton;
        private System.Windows.Forms.Button movePreviousButton;
        private System.Windows.Forms.Button mySaveButton;
        private System.Windows.Forms.DateTimePicker ngayXuatBanTimePicker;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox noiDungTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox soTrangTextBox;
        private System.Windows.Forms.TextBox khoSachTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox lanXuatBanTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button myAddButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button mySearchButton;
        private System.Windows.Forms.TextBox mySearchTextBox;
        private System.Windows.Forms.TextBox tenSachTextBox;
        private System.Windows.Forms.TextBox ISBNTextBox;
        private System.Windows.Forms.Label maNgonNguLabel;
        private System.Windows.Forms.DataGridView myDataGridView;
        private System.Windows.Forms.Label tenNgonNguLabel;
        private System.Windows.Forms.TextBox hinhAnhPathTextBox;
        private System.Windows.Forms.TextBox giaTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox maNgonNguComboBox;
        private System.Windows.Forms.ComboBox maTheLoaiComboBox;
        private System.Windows.Forms.TextBox tenNgonNguTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tenTheLoaiTextBox;

    }
}