﻿namespace QuanLiThuVien
{
    partial class NganTuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.moveFirstButton = new System.Windows.Forms.Button();
            this.moveNextButton = new System.Windows.Forms.Button();
            this.MoveLastButton = new System.Windows.Forms.Button();
            this.movePreviousButton = new System.Windows.Forms.Button();
            this.mySearchButton = new System.Windows.Forms.Button();
            this.mySearchTextBox = new System.Windows.Forms.TextBox();
            this.moTaTextBox = new System.Windows.Forms.TextBox();
            this.tenNgonNguLabel = new System.Windows.Forms.Label();
            this.maNganTuTextBox = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.myDataGridView = new System.Windows.Forms.DataGridView();
            this.myEditButton = new System.Windows.Forms.Button();
            this.myAddButton = new System.Windows.Forms.Button();
            this.myCancelButton = new System.Windows.Forms.Button();
            this.mySaveButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.myDeleteButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.myDataGridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.moveFirstButton);
            this.groupBox1.Controls.Add(this.moveNextButton);
            this.groupBox1.Controls.Add(this.MoveLastButton);
            this.groupBox1.Controls.Add(this.movePreviousButton);
            this.groupBox1.Location = new System.Drawing.Point(94, 198);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(149, 44);
            this.groupBox1.TabIndex = 73;
            this.groupBox1.TabStop = false;
            // 
            // moveFirstButton
            // 
            this.moveFirstButton.Location = new System.Drawing.Point(6, 11);
            this.moveFirstButton.Name = "moveFirstButton";
            this.moveFirstButton.Size = new System.Drawing.Size(30, 23);
            this.moveFirstButton.TabIndex = 11;
            this.moveFirstButton.Text = "<<";
            this.moveFirstButton.UseVisualStyleBackColor = true;
            this.moveFirstButton.Click += new System.EventHandler(this.moveFirstButton_Click);
            // 
            // moveNextButton
            // 
            this.moveNextButton.Location = new System.Drawing.Point(78, 11);
            this.moveNextButton.Name = "moveNextButton";
            this.moveNextButton.Size = new System.Drawing.Size(30, 23);
            this.moveNextButton.TabIndex = 14;
            this.moveNextButton.Text = ">";
            this.moveNextButton.UseVisualStyleBackColor = true;
            this.moveNextButton.Click += new System.EventHandler(this.MoveLastButton_Click);
            // 
            // MoveLastButton
            // 
            this.MoveLastButton.Location = new System.Drawing.Point(114, 11);
            this.MoveLastButton.Name = "MoveLastButton";
            this.MoveLastButton.Size = new System.Drawing.Size(30, 23);
            this.MoveLastButton.TabIndex = 12;
            this.MoveLastButton.Text = ">>";
            this.MoveLastButton.UseVisualStyleBackColor = true;
            this.MoveLastButton.Click += new System.EventHandler(this.MoveLastButton_Click);
            // 
            // movePreviousButton
            // 
            this.movePreviousButton.Location = new System.Drawing.Point(42, 11);
            this.movePreviousButton.Name = "movePreviousButton";
            this.movePreviousButton.Size = new System.Drawing.Size(30, 23);
            this.movePreviousButton.TabIndex = 13;
            this.movePreviousButton.Text = "<";
            this.movePreviousButton.UseVisualStyleBackColor = true;
            this.movePreviousButton.Click += new System.EventHandler(this.movePreviousButton_Click);
            // 
            // mySearchButton
            // 
            this.mySearchButton.Location = new System.Drawing.Point(643, 13);
            this.mySearchButton.Name = "mySearchButton";
            this.mySearchButton.Size = new System.Drawing.Size(75, 23);
            this.mySearchButton.TabIndex = 72;
            this.mySearchButton.Text = "Tìm kiếm";
            this.mySearchButton.UseVisualStyleBackColor = true;
            this.mySearchButton.Click += new System.EventHandler(this.mySearchButton_Click);
            // 
            // mySearchTextBox
            // 
            this.mySearchTextBox.Location = new System.Drawing.Point(510, 16);
            this.mySearchTextBox.Name = "mySearchTextBox";
            this.mySearchTextBox.Size = new System.Drawing.Size(127, 20);
            this.mySearchTextBox.TabIndex = 71;
            // 
            // moTaTextBox
            // 
            this.moTaTextBox.Enabled = false;
            this.moTaTextBox.Location = new System.Drawing.Point(162, 296);
            this.moTaTextBox.Name = "moTaTextBox";
            this.moTaTextBox.Size = new System.Drawing.Size(149, 20);
            this.moTaTextBox.TabIndex = 70;
            // 
            // tenNgonNguLabel
            // 
            this.tenNgonNguLabel.AutoSize = true;
            this.tenNgonNguLabel.Location = new System.Drawing.Point(91, 303);
            this.tenNgonNguLabel.Name = "tenNgonNguLabel";
            this.tenNgonNguLabel.Size = new System.Drawing.Size(37, 13);
            this.tenNgonNguLabel.TabIndex = 69;
            this.tenNgonNguLabel.Text = "Mô tả:";
            // 
            // maNganTuTextBox
            // 
            this.maNganTuTextBox.Enabled = false;
            this.maNganTuTextBox.Location = new System.Drawing.Point(162, 261);
            this.maNganTuTextBox.Name = "maNganTuTextBox";
            this.maNganTuTextBox.Size = new System.Drawing.Size(149, 20);
            this.maNganTuTextBox.TabIndex = 68;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(91, 264);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(64, 13);
            this.label.TabIndex = 67;
            this.label.Text = "Mã ngăn tủ:";
            // 
            // myDataGridView
            // 
            this.myDataGridView.AllowUserToAddRows = false;
            this.myDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.myDataGridView.Location = new System.Drawing.Point(94, 42);
            this.myDataGridView.Name = "myDataGridView";
            this.myDataGridView.ReadOnly = true;
            this.myDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.myDataGridView.Size = new System.Drawing.Size(624, 150);
            this.myDataGridView.TabIndex = 66;
            this.myDataGridView.SelectionChanged += new System.EventHandler(this.myDataGridView_SelectionChanged);
            // 
            // myEditButton
            // 
            this.myEditButton.Location = new System.Drawing.Point(118, 19);
            this.myEditButton.Name = "myEditButton";
            this.myEditButton.Size = new System.Drawing.Size(100, 23);
            this.myEditButton.TabIndex = 8;
            this.myEditButton.Text = "Sửa";
            this.myEditButton.UseVisualStyleBackColor = true;
            this.myEditButton.Click += new System.EventHandler(this.myEditButton_Click);
            // 
            // myAddButton
            // 
            this.myAddButton.Location = new System.Drawing.Point(6, 19);
            this.myAddButton.Name = "myAddButton";
            this.myAddButton.Size = new System.Drawing.Size(100, 23);
            this.myAddButton.TabIndex = 6;
            this.myAddButton.Text = "Thêm";
            this.myAddButton.UseVisualStyleBackColor = true;
            this.myAddButton.Click += new System.EventHandler(this.myAddButton_Click);
            // 
            // myCancelButton
            // 
            this.myCancelButton.Enabled = false;
            this.myCancelButton.Location = new System.Drawing.Point(162, 49);
            this.myCancelButton.Name = "myCancelButton";
            this.myCancelButton.Size = new System.Drawing.Size(100, 23);
            this.myCancelButton.TabIndex = 17;
            this.myCancelButton.Text = "Bỏ qua";
            this.myCancelButton.UseVisualStyleBackColor = true;
            this.myCancelButton.Click += new System.EventHandler(this.myCancelButton_Click);
            // 
            // mySaveButton
            // 
            this.mySaveButton.Enabled = false;
            this.mySaveButton.Location = new System.Drawing.Point(56, 50);
            this.mySaveButton.Name = "mySaveButton";
            this.mySaveButton.Size = new System.Drawing.Size(100, 23);
            this.mySaveButton.TabIndex = 16;
            this.mySaveButton.Text = "Ghi";
            this.mySaveButton.UseVisualStyleBackColor = true;
            this.mySaveButton.Click += new System.EventHandler(this.mySaveButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.myAddButton);
            this.groupBox2.Controls.Add(this.myCancelButton);
            this.groupBox2.Controls.Add(this.mySaveButton);
            this.groupBox2.Controls.Add(this.myEditButton);
            this.groupBox2.Controls.Add(this.myDeleteButton);
            this.groupBox2.Location = new System.Drawing.Point(378, 209);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(340, 83);
            this.groupBox2.TabIndex = 74;
            this.groupBox2.TabStop = false;
            // 
            // myDeleteButton
            // 
            this.myDeleteButton.Location = new System.Drawing.Point(230, 19);
            this.myDeleteButton.Name = "myDeleteButton";
            this.myDeleteButton.Size = new System.Drawing.Size(100, 23);
            this.myDeleteButton.TabIndex = 7;
            this.myDeleteButton.Text = "Xóa";
            this.myDeleteButton.UseVisualStyleBackColor = true;
            this.myDeleteButton.Click += new System.EventHandler(this.myDeleteButton_Click);
            // 
            // NganTuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 614);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.mySearchButton);
            this.Controls.Add(this.mySearchTextBox);
            this.Controls.Add(this.moTaTextBox);
            this.Controls.Add(this.tenNgonNguLabel);
            this.Controls.Add(this.maNganTuTextBox);
            this.Controls.Add(this.label);
            this.Controls.Add(this.myDataGridView);
            this.Controls.Add(this.groupBox2);
            this.Name = "NganTuForm";
            this.Text = "NganTuForm";
            this.Load += new System.EventHandler(this.myForm_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.myDataGridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button moveFirstButton;
        private System.Windows.Forms.Button moveNextButton;
        private System.Windows.Forms.Button MoveLastButton;
        private System.Windows.Forms.Button movePreviousButton;
        private System.Windows.Forms.Button mySearchButton;
        private System.Windows.Forms.TextBox mySearchTextBox;
        private System.Windows.Forms.TextBox moTaTextBox;
        private System.Windows.Forms.Label tenNgonNguLabel;
        private System.Windows.Forms.TextBox maNganTuTextBox;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.DataGridView myDataGridView;
        private System.Windows.Forms.Button myEditButton;
        private System.Windows.Forms.Button myAddButton;
        private System.Windows.Forms.Button myCancelButton;
        private System.Windows.Forms.Button mySaveButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button myDeleteButton;
    }
}