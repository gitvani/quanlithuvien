﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLiThuVien
{
    public partial class DocGiaForm : Form
    {
        public DocGiaForm()
        {
            InitializeComponent();
        }
        private void myForm_Load(object sender, EventArgs e)
        {
            getDataList();
        }
        // TODO: change
        private const String SPSelect = "DOCGIA_XEM";
        private const String SPInsert = "DOCGIA_THEM";
        private const String SPUpdate = "DOCGIA_SUA";
        private const String SPDelete = "DOCGIA_XOA";
        private const Boolean isAutoIncreasePrimaryKey = true;
        // ---------------------------------
        private void getDataList()
        {
            myDataGridView = GlobalFunction.getDataList(myDataGridView, SPSelect);

            // get data for maNgonNgu Combobox 

        }




        private String action = "";
        private void myAddButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.AddNew();
            action = "Add";
            enableActionForAddAndEditButton();
        }
        private void myEditButton_Click(object sender, EventArgs e)
        {
            action = "Edit";
            enableActionForAddAndEditButton();
        }

        private void myDeleteButton_Click(object sender, EventArgs e)
        {
            // TODO: change
            String primaryKey = maDocGiaTextBox.Text;
            GlobalFunction.deleteRow(SPDelete, primaryKey, "Mã Độc giả");
            // -----------------------------
            getDataList();

        }
        private void myDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in myDataGridView.SelectedRows)
            {
                // TODO: change
                try
                {
                    maDocGiaTextBox.Text = row.Cells[0].Value.ToString();
                    hoTextBox.Text = row.Cells[1].Value.ToString();
                    tenTextBox.Text = row.Cells[2].Value.ToString();
                    emailTextBox.Text = row.Cells[3].Value.ToString();
                    soCMNDTextBox.Text = row.Cells[4].Value.ToString();
                    gioiTinhComboBox.SelectedIndex = (row.Cells[5].Value.ToString().Equals("False") ? 0 : 1);
                    if (row.Cells[6].Value.ToString() != String.Empty)
                    {
                        ngaySinhDateTimePicker.Value = Convert.ToDateTime(row.Cells[6].Value.ToString());
                    }
                    
                    diaChiTextBox.Text = row.Cells[7].Value.ToString();
                    dienThoaiTextBox.Text = row.Cells[8].Value.ToString();
                    if (row.Cells[9].Value.ToString() != String.Empty)
                    {
                        ngayLamTheDateTimePicker.Value = DateTime.Parse(row.Cells[9].Value.ToString());
                    }
                    else
                    {
                        ngayLamTheDateTimePicker.Value = DateTime.Today; 
                    }
                    if (row.Cells[10].Value.ToString() != String.Empty)
                    {
                        ngayHetHanDateTimePicker.Value = DateTime.Parse(row.Cells[10].Value.ToString());
                    }
                    else
                    {
                        ngayHetHanDateTimePicker.Value = ngayLamTheDateTimePicker.Value.Add( new TimeSpan(365,0,0,0)); // add 365 days
                    }
                  
                  
                    hoatDongComboBox.SelectedIndex = (row.Cells[11].Value.ToString().Equals("False") ? 0 : 1);
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message); 
                }
            



                //------------------------------------------------------------

            }


        }

        private void mySaveButton_Click(object sender, EventArgs e)
        {
            // TODO: change ----------------------------------------------

            // ràng buộc 
            if( DateTime.Compare(ngaySinhDateTimePicker.Value , DateTime.Today) > 0 ) { 
                MessageBox.Show("Ngày sinh phải bé hơn " + DateTime.Today , "Lỗi" , MessageBoxButtons.OK) ; 
                return ; 
            }

            if (DateTime.Compare(ngayLamTheDateTimePicker.Value, ngayHetHanDateTimePicker.Value) > 0)
            {
                MessageBox.Show("Ngày hết hạn phải lớn hơn ngày làm thẻ.", "Lỗi", MessageBoxButtons.OK);
                return;
            }  

            ///////////////////////

            String primaryKey = maDocGiaTextBox.Text;
            String ho = hoTextBox.Text;
            String ten = tenTextBox.Text;
            String email = emailTextBox.Text;
            String cmnd = soCMNDTextBox.Text;
            String gioiTinh = gioiTinhComboBox.SelectedIndex.ToString();
            String ngaySinh = ngaySinhDateTimePicker.Value.ToString("yyyy-MM-dd"); 
            String diaChi = diaChiTextBox.Text;
            String dienThoai = dienThoaiTextBox.Text;
            String ngayLamThe = ngayLamTheDateTimePicker.Value.ToString("yyyy-MM-dd");
            String ngayHetHan = ngayHetHanDateTimePicker.Value.ToString("yyyy-MM-dd");
            String hoatDong = hoatDongComboBox.SelectedIndex.ToString(); 

            List<String> textBoxValues = new List<String>();
            textBoxValues.Add(primaryKey);
            textBoxValues.Add(ho);
            textBoxValues.Add(ten);
            textBoxValues.Add(email);
            textBoxValues.Add(cmnd);
            textBoxValues.Add(gioiTinh);
            textBoxValues.Add(ngaySinh);
            textBoxValues.Add(diaChi);
            textBoxValues.Add(dienThoai);
            textBoxValues.Add(ngayLamThe);
            textBoxValues.Add(ngayHetHan);
            textBoxValues.Add(hoatDong); 

            List<String> lableForTextBoxValues = new List<String>();
            lableForTextBoxValues.Add("Mã độc giả");
            lableForTextBoxValues.Add("Họ");
            lableForTextBoxValues.Add("Tên");
            lableForTextBoxValues.Add("email");
            lableForTextBoxValues.Add("CMND");
            lableForTextBoxValues.Add("Giới tính");
            lableForTextBoxValues.Add("Ngày sinh");
            lableForTextBoxValues.Add("Địa chỉ");
            lableForTextBoxValues.Add("Điện thoại");
            lableForTextBoxValues.Add("Ngày làm thẻ");
            lableForTextBoxValues.Add("Ngày hết hạn");
            lableForTextBoxValues.Add("Hoạt động");

            List<int> primaryKeyIndex = new List<int>();
            primaryKeyIndex.Add(0);

        
            myDataGridView = GlobalFunction.saveActionAuto(myDataGridView, isAutoIncreasePrimaryKey, primaryKeyIndex, textBoxValues, lableForTextBoxValues, action, SPInsert, SPUpdate, SPSelect);
            enableActionForSaveAndCancelButton();

        }

        private void myCancelButton_Click(object sender, EventArgs e)
        {
            GlobalFunction.cancelAciton();
         

            enableActionForSaveAndCancelButton();
        }

        private void mySearchButton_Click(object sender, EventArgs e)
        {
            GlobalFunction.searchData(mySearchTextBox.Text, myDataGridView);
        }

        private void moveFirstButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveFirst();
        }

        private void MoveLastButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveLast();
        }

        private void movePreviousButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MovePrevious();
        }

        private void moveNextButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveNext();
        }


        private void enableActionForSaveAndCancelButton()
        {
            // TODO: change
            // diable textbox
            maDocGiaTextBox.Enabled = false;
            hoTextBox.Enabled = false;
            tenTextBox.Enabled = false;
            emailTextBox.Enabled = false;
            soCMNDTextBox.Enabled = false;
            gioiTinhComboBox.Enabled = false;        
            ngaySinhDateTimePicker.Enabled = false;
            diaChiTextBox.Enabled = false;
            dienThoaiTextBox.Enabled = false;
            ngayHetHanDateTimePicker.Enabled = false;
            ngayLamTheDateTimePicker.Enabled = false;
            hoatDongComboBox.Enabled = false;
           
            ///////////////////////////////////////
            // config button
            myCancelButton.Enabled = false;
            mySaveButton.Enabled = false;
            myAddButton.Enabled = true;
            myEditButton.Enabled = true;
            myDeleteButton.Enabled = true;
            myDataGridView.Enabled = true;
        }
        private void enableActionForAddAndEditButton()
        {
            // TODO: change     
            //config textbox
            if (!isAutoIncreasePrimaryKey)
            {
                maDocGiaTextBox.Enabled = true;
            }
            else
            {
                maDocGiaTextBox.Enabled = false;
            }

            hoTextBox.Enabled = true;
            tenTextBox.Enabled = true;
            emailTextBox.Enabled = true;
            soCMNDTextBox.Enabled = true;
            gioiTinhComboBox.Enabled = true;
            ngaySinhDateTimePicker.Enabled = true;
            diaChiTextBox.Enabled = true;
            dienThoaiTextBox.Enabled = true;
            ngayHetHanDateTimePicker.Enabled = true;
            ngayLamTheDateTimePicker.Enabled = true;
            hoatDongComboBox.Enabled = true;
            //////////////////////////////////////////
            // config button
            mySaveButton.Enabled = true;
            myCancelButton.Enabled = true;
            myDataGridView.Enabled = false;
            myDeleteButton.Enabled = false;
            myAddButton.Enabled = false;
            myEditButton.Enabled = false;
            maDocGiaTextBox.Focus();

        }

       
    }
}
