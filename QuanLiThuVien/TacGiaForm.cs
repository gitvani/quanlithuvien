﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLiThuVien
{
    public partial class TacGiaForm : Form
    {
        public TacGiaForm()
        {
            InitializeComponent();
        }
        private void myForm_Load(object sender, EventArgs e)
        {
            getDataList();
        }
        // TODO: change
        private const String SPSelect = "TACGIA_XEM";
        private const String SPInsert = "TACGIA_THEM";
        private const String SPUpdate = "TACGIA_SUA";
        private const String SPDelete = "TACGIA_XOA";
        private const Boolean isAutoIncreasePrimaryKey = true;
        // ---------------------------------
        private void getDataList()
        {
            myDataGridView = GlobalFunction.getDataList(myDataGridView, SPSelect);
        }




        private String action = "";
        private void myAddButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.AddNew();
            action = "Add";
            enableActionForAddAndEditButton();
        }
        private void myEditButton_Click(object sender, EventArgs e)
        {
            action = "Edit";
            enableActionForAddAndEditButton();
        }

        private void myDeleteButton_Click(object sender, EventArgs e)
        {
            // TODO: change
            String primaryKey = maTacGiaTextBox.Text;
            GlobalFunction.deleteRow(SPDelete, primaryKey, "Mã tác giả");
            // -----------------------------
            getDataList();

        }
        private void myDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in myDataGridView.SelectedRows)
            {
                // TODO: change
                maTacGiaTextBox.Text = row.Cells[0].Value.ToString();
                hoTenNhanVienTextBox.Text = row.Cells[1].Value.ToString();
                diaChiTextBox.Text = row.Cells[2].Value.ToString();
                dienThoaiTextBox.Text = row.Cells[3].Value.ToString();
              



                //------------------------------------------------------------

            }


        }

        private void mySaveButton_Click(object sender, EventArgs e)
        {
            // TODO: change ----------------------------------------------
            String primaryKey = maTacGiaTextBox.Text;
            String hoTenTacGia = hoTenNhanVienTextBox.Text;
            String diaChi = diaChiTextBox.Text;
            String dienThoai = dienThoaiTextBox.Text;
         

            List<String> textBoxValues = new List<String>();
            textBoxValues.Add(primaryKey);
            textBoxValues.Add(hoTenTacGia);
            textBoxValues.Add(diaChi);
            textBoxValues.Add(dienThoai);
        

            List<String> lableForTextBoxValues = new List<String>();
            lableForTextBoxValues.Add("Mã tác giả");
            lableForTextBoxValues.Add("Họ tên");
            lableForTextBoxValues.Add("Địa chỉ");
            lableForTextBoxValues.Add("Điện thoại");
           
            List<int> primaryKeyIndex = new List<int>();
            primaryKeyIndex.Add(0);


            myDataGridView = GlobalFunction.saveActionAuto(myDataGridView, isAutoIncreasePrimaryKey, primaryKeyIndex, textBoxValues, lableForTextBoxValues, action, SPInsert, SPUpdate, SPSelect);
            //----------------------------------------------     
            enableActionForSaveAndCancelButton();

        }

        private void myCancelButton_Click(object sender, EventArgs e)
        {
            GlobalFunction.cancelAciton();
            enableActionForSaveAndCancelButton();
        }

        private void mySearchButton_Click(object sender, EventArgs e)
        {
            GlobalFunction.searchData(mySearchTextBox.Text, myDataGridView);
        }

        private void moveFirstButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveFirst();
        }

        private void MoveLastButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveLast();
        }

        private void movePreviousButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MovePrevious();
        }

        private void moveNextButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveNext();
        }


        private void enableActionForSaveAndCancelButton()
        {
            // TODO: change
            // diable textbox
            maTacGiaTextBox.Enabled = false;
            hoTenNhanVienTextBox.Enabled = false;
         
            diaChiTextBox.Enabled = false;
            dienThoaiTextBox.Enabled = false;
         
           
            ///////////////////////////////////////
            // config button
            myCancelButton.Enabled = false;
            mySaveButton.Enabled = false;
            myAddButton.Enabled = true;
            myEditButton.Enabled = true;
            myDeleteButton.Enabled = true;
            myDataGridView.Enabled = true;
        }
        private void enableActionForAddAndEditButton()
        {
            // TODO: change     
            //config textbox
            if (action == "Add" && !isAutoIncreasePrimaryKey)
            {
                maTacGiaTextBox.Enabled = true;
            }
            else
            {
                maTacGiaTextBox.Enabled = false;
            }

            hoTenNhanVienTextBox.Enabled = true;
            diaChiTextBox.Enabled = true;
            dienThoaiTextBox.Enabled = true;
         
            //////////////////////////////////////////
            // config button
            mySaveButton.Enabled = true;
            myCancelButton.Enabled = true;
            myDataGridView.Enabled = false;
            myDeleteButton.Enabled = false;
            myAddButton.Enabled = false;
            myEditButton.Enabled = false;
            maTacGiaTextBox.Focus();

        }
    }
}
