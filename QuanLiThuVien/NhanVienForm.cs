﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLiThuVien
{
    public partial class NhanVienForm : Form
    {
        public NhanVienForm()
        {
            InitializeComponent();
        }

        private void myForm_Load(object sender, EventArgs e)
        {
            getDataList();
        }
        // TODO: change
        private const String SPSelect = "NHANVIEN_XEM";
        private const String SPInsert = "NHANVIEN_THEM";
        private const String SPUpdate = "NHANVIEN_SUA";
        private const String SPDelete = "NHANVIEN_XOA";
        private const Boolean isAutoIncreasePrimaryKey = true;
        // ---------------------------------
        private void getDataList()
        {
            myDataGridView = GlobalFunction.getDataList(myDataGridView, SPSelect);
        }

        


        private String action = "";
        private void myAddButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.AddNew();
            action = "Add";
            enableActionForAddAndEditButton();
        }
        private void myEditButton_Click(object sender, EventArgs e)
        {
            action = "Edit";
            enableActionForAddAndEditButton();
        }

        private void myDeleteButton_Click(object sender, EventArgs e)
        {
            // TODO: change
            String primaryKey = maNhanVienTextBox.Text;
            GlobalFunction.deleteRow(SPDelete, primaryKey, "Mã nhân viên");
            // -----------------------------
            getDataList();

        }
        private void myDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in myDataGridView.SelectedRows)
            {
                // TODO: change
                maNhanVienTextBox.Text = row.Cells[0].Value.ToString();
                hoNhanVienTextBox.Text = row.Cells[1].Value.ToString();
                tenNhanVienTextBox.Text = row.Cells[2].Value.ToString();
                gioiTinhComboBox.SelectedIndex = (row.Cells[3].Value.ToString().Equals("False") ? 0 : 1); 
                diaChiTextBox.Text = row.Cells[4].Value.ToString();
                dienThoaiTextBox.Text = row.Cells[5].Value.ToString();
                emailTextBox.Text = row.Cells[6].Value.ToString();


               
                //------------------------------------------------------------

            }


        }

        private void mySaveButton_Click(object sender, EventArgs e)
        {
            // TODO: change ----------------------------------------------
            String primaryKey = maNhanVienTextBox.Text;
            String hoNhanVien = hoNhanVienTextBox.Text;
            String tenNhanVien = tenNhanVienTextBox.Text;
            String gioiTinh = gioiTinhComboBox.SelectedItem.ToString();
            String diaChi = diaChiTextBox.Text;
            String dienThoai = dienThoaiTextBox.Text;
            String email = emailTextBox.Text;

            List<String> textBoxValues = new List<String>();
            textBoxValues.Add(primaryKey);
            textBoxValues.Add(hoNhanVien); 
            textBoxValues.Add(tenNhanVien);
            textBoxValues.Add(gioiTinh);
            textBoxValues.Add(diaChi);
            textBoxValues.Add(dienThoai);
            textBoxValues.Add(email);

            List<String> lableForTextBoxValues = new List<String>();
            lableForTextBoxValues.Add("Mã nhân viên");
            lableForTextBoxValues.Add("Họ nhân viên");
            lableForTextBoxValues.Add("Tên nhân viên");
            lableForTextBoxValues.Add("Giới tính");
            lableForTextBoxValues.Add("Địa chỉ");
            lableForTextBoxValues.Add("Điện thoại");
            lableForTextBoxValues.Add("Email");
          
            List<int> primaryKeyIndex = new List<int>();
            primaryKeyIndex.Add(0);

          
            myDataGridView = GlobalFunction.saveActionAuto(myDataGridView, isAutoIncreasePrimaryKey, primaryKeyIndex, textBoxValues, lableForTextBoxValues, action, SPInsert, SPUpdate, SPSelect);
            //----------------------------------------------     
            enableActionForSaveAndCancelButton();

        }

        private void myCancelButton_Click(object sender, EventArgs e)
        {
            GlobalFunction.cancelAciton();
            enableActionForSaveAndCancelButton();
        }

        private void mySearchButton_Click(object sender, EventArgs e)
        {
            GlobalFunction.searchData(mySearchTextBox.Text, myDataGridView);
        }

        private void moveFirstButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveFirst();
        }

        private void MoveLastButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveLast();
        }

        private void movePreviousButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MovePrevious();
        }

        private void moveNextButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveNext();
        }


        private void enableActionForSaveAndCancelButton()
        {
            // TODO: change
            // diable textbox
            maNhanVienTextBox.Enabled = false;
            hoNhanVienTextBox.Enabled = false;
            tenNhanVienTextBox.Enabled = false;
          
            diaChiTextBox.Enabled = false;
            dienThoaiTextBox.Enabled = false;
            emailTextBox.Enabled = false;
            gioiTinhComboBox.Enabled = false; 
            ///////////////////////////////////////
            // config button
            myCancelButton.Enabled = false;
            mySaveButton.Enabled = false;
            myAddButton.Enabled = true;
            myEditButton.Enabled = true;
            myDeleteButton.Enabled = true;
            myDataGridView.Enabled = true;
        }
        private void enableActionForAddAndEditButton()
        {
            // TODO: change     
            //config textbox
            if (action == "Add" && !isAutoIncreasePrimaryKey)
            {
                maNhanVienTextBox.Enabled = true;
            }
            else
            {
                maNhanVienTextBox.Enabled = false;
            }

            hoNhanVienTextBox.Enabled = true;
            tenNhanVienTextBox.Enabled = true;
          
            diaChiTextBox.Enabled = true;
            dienThoaiTextBox.Enabled = true;
            emailTextBox.Enabled = true;
            gioiTinhComboBox.Enabled = true; 
            //////////////////////////////////////////
            // config button
            mySaveButton.Enabled = true;
            myCancelButton.Enabled = true;
            myDataGridView.Enabled = false;
            myDeleteButton.Enabled = false;
            myAddButton.Enabled = false;
            myEditButton.Enabled = false;
            maNhanVienTextBox.Focus();

        }
    }
}
