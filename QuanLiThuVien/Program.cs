﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLiThuVien
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LoginForm());
        }
         public static SqlConnection conn = new SqlConnection();
        public static String connectionString;
        //public static SqlDataAdapter dataAdapter;
        public static SqlDataReader myReader;
        public static String serverName = "";
        public static String username;
        public static String fullName;
        public static String role;
        public static String password;
        public static String database = "QUANLITHUVIEN";
        public static String mlogin;
        public static BindingSource bindingSource; 
        //public static String mGroup;
        //public static String mHoten;
          public static int KetNoi()
        {
            if (Program.conn != null && Program.conn.State == ConnectionState.Open) Program.conn.Close();
            try
            {
                Program.connectionString = "Server=" + Program.serverName + ";Database=" + Program.database + ";User Id=" +
                      Program.mlogin + ";password=" + Program.password;
                Console.WriteLine(Program.connectionString); 
                Program.conn.ConnectionString = Program.connectionString;
                Program.conn.Open();
                return 1;
            }

            catch (Exception e)
            {
                MessageBox.Show("Lỗi kết nối cơ sở dữ liệu.\nBạn xem lại user name và password.\n " + e.Message, "", MessageBoxButtons.OK);
                return 0;
            }
        }
       
        public static SqlDataReader ExecSqlDataReader(String cmd, String connectionstring)
        {
            SqlDataReader myreader;
            //Program.conn = new SqlConnection(connectionstring);

            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.Connection = Program.conn;
            sqlcmd.CommandText = cmd;
            sqlcmd.CommandType = CommandType.Text;

            if (Program.conn.State == ConnectionState.Closed) Program.conn.Open();
            try
            {
                myreader = sqlcmd.ExecuteReader(); return myreader;
            }
            catch (SqlException ex)
            {
                Program.conn.Close();
                MessageBox.Show(ex.Message ,"Truy vấn SQL thất bại" , MessageBoxButtons.OK);
                return null;
            }
        }

        public static BindingSource getBindingSource(String selectCommand)
        {
            BindingSource bindingSource = new BindingSource();
            try
            {
                SqlDataAdapter dataAdapter = new SqlDataAdapter(selectCommand, Program.connectionString);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                dataAdapter.Fill(table);
                bindingSource.DataSource = table;
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            return bindingSource;
          

        }
        public static void GetData(string selectCommand)
        {
            try
            {
                // Specify a connection string. Replace the given value with a 
                // valid connection string for a Northwind SQL Server sample
                // database accessible to your system.
                String connectionString = Program.connectionString; 

                  
                // Create a new data adapter based on the specified query.
                SqlDataAdapter dataAdapter = new SqlDataAdapter(selectCommand, connectionString);

                // Create a command builder to generate SQL update, insert, and
                // delete commands based on selectCommand. These are used to
                // update the database.
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);

                // Populate a new data table and bind it to the BindingSource.
                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                dataAdapter.Fill(table);
                bindingSource = new BindingSource();
                bindingSource.DataSource = table;

                // Resize the DataGridView columns to fit the newly loaded content.
               
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Lỗi SQL: " + ex.Message );
            }
        }
        public static void editData(String sql)
        {
            Console.WriteLine("test: " + sql); 
            System.Data.SqlClient.SqlConnection sqlConnection =
                new System.Data.SqlClient.SqlConnection(Program.connectionString);  
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = sql;
            cmd.Connection = sqlConnection;

            sqlConnection.Open();
            cmd.ExecuteNonQuery();
            sqlConnection.Close();

          
        
        }

        
        
    }
}
