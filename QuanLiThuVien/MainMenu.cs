﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLiThuVien
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void MainMenu_Load(object sender, EventArgs e)
        {

        }

        private void iSBNToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ISBNForm ISBNForm = new ISBNForm();
            loadSubForm(ISBNForm);
            //ISBNForm.TopLevel = false; 

            this.panelMain.Controls.Add(ISBNForm);
            ISBNForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            ISBNForm.Dock = DockStyle.Fill;
            ISBNForm.Show();
        }
       
        private void loadSubForm(Form form) 
        {

            this.panelMain.Controls.Clear(); //to remove all controls
                
            form.TopLevel = false;

            this.panelMain.Controls.Add(form);
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            form.Dock = DockStyle.Fill;
            form.Show();

        }

        private void theLoaiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TheLoaiForm TheLoaiForm = new TheLoaiForm();
            loadSubForm(TheLoaiForm);
        }

        private void quanLiTaiKhoanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AccountInfomationForm AccountInfomationForm = new AccountInfomationForm();
            loadSubForm(AccountInfomationForm); 
        }

        private void ngonNguToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NgonNguForm NgonNguForm = new NgonNguForm();
            loadSubForm(NgonNguForm);
        }

        private void nhanVienToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NhanVienForm NhanVienForm = new NhanVienForm();
            loadSubForm(NhanVienForm); 
        }

        private void độcGiảToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DocGiaForm DocGiaForm = new DocGiaForm();
            loadSubForm(DocGiaForm); 
        }

        private void iSBNToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ISBNForm ISBNForm = new ISBNForm();
            loadSubForm(ISBNForm);
        }

        private void tacGiaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TacGiaForm TacGiaForm = new TacGiaForm();
            loadSubForm(TacGiaForm); 
        }

        private void sachToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SachForm SachForm = new SachForm();
            loadSubForm(SachForm); 
        }

        private void nganTuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NganTuForm NganTuForm = new NganTuForm();
            loadSubForm(NganTuForm); 
        }

        private void sachDienTuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SachDienTuForm sachDienTu = new SachDienTuForm();
            loadSubForm(sachDienTu); 
        }

        private void mượnSáchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MuonSach MuonSach = new MuonSach();
            loadSubForm(MuonSach); 
        }

        private void trảSáchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TraSachForm TraSachForm = new TraSachForm();
            loadSubForm(TraSachForm); 
        }

       
    }
}
