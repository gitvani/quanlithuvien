﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLiThuVien
{
    public partial class SachForm : Form
    {
        public SachForm()
        {
            InitializeComponent();
        }
        private void myForm_Load(object sender, EventArgs e)
        {
            
            getDataList();
        }
        // TODO: change
        private const String SPSelect = "SACH_XEM";
        private const String SPInsert = "SACH_THEM";
        private const String SPUpdate = "SACH_SUA";
        private const String SPDelete = "SACH_XOA";
        private const Boolean isAutoIncreasePrimaryKey = false;


        private BindingSource ISBNBindingSource;
        private BindingSource nganTuBindingSource;
        // ---------------------------------
        private void getDataList()
        {
            myDataGridView = GlobalFunction.getDataList(myDataGridView, SPSelect);



            // get data for nganTu ComboBox
            maNganTuComboBox.Items.Clear(); 
            nganTuBindingSource = Program.getBindingSource("exec NGANTU_XEM");
            for (int i = 0; i < nganTuBindingSource.List.Count; i++)
            {
                DataRowView current = (DataRowView)nganTuBindingSource.List[i];
                maNganTuComboBox.Items.Add(current["MANGANTU"].ToString());
            }

            // get data for ISNB ComboBox
            ISBNComboBox.Items.Clear();
            ISBNBindingSource = Program.getBindingSource("exec ISBN_XEM");

            for (int i = 0; i < ISBNBindingSource.List.Count; i++)
            {
                DataRowView current = (DataRowView)ISBNBindingSource.List[i];
                ISBNComboBox.Items.Add(current["ISBN"].ToString().Trim());
              
            }
              



        }




        private String action = "";
        private void myAddButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.AddNew();
            action = "Add";
            enableActionForAddAndEditButton();
        }
        private void myEditButton_Click(object sender, EventArgs e)
        {
            action = "Edit";
            if (!isAutoIncreasePrimaryKey)
            {
                GlobalFunction.oldPrimaryKey = maSachTextBox.Text;
            }

            enableActionForAddAndEditButton();
        }

        private void myDeleteButton_Click(object sender, EventArgs e)
        {
            // TODO: change
            String primaryKey = maSachTextBox.Text;
            GlobalFunction.deleteRow(SPDelete, primaryKey, "Mã Độc giả");
            // -----------------------------
            getDataList();

        }
        private void myDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in myDataGridView.SelectedRows)
            {
                // TODO: change
                try
                {
                   
                    ISBNComboBox.SelectedItem = row.Cells[0].Value.ToString();               
                    maSachTextBox.Text = row.Cells[1].Value.ToString();
                    tinhTrangComboBox.SelectedItem = GlobalFunction.convertBitToString(row.Cells[2].Value.ToString(), "Cũ", "Mới");
                    choMuonComboBox.SelectedItem = GlobalFunction.convertBitToString(row.Cells[3].Value.ToString(), "Chưa Cho Mượn", "Đã Cho Mượn");
                    maNganTuComboBox.SelectedItem = row.Cells[4].Value.ToString(); 
                }
                catch (Exception)
                {
                   // MessageBox.Show(exc.Message ,"Lỗi load dữ liệu DataGridView" , MessageBoxButtons.OK);
                }




                //------------------------------------------------------------

            }


        }

        private void mySaveButton_Click(object sender, EventArgs e)
        {
            // TODO: change ----------------------------------------------


            String ISBN = ISBNComboBox.SelectedItem.ToString();
            String maSach = maSachTextBox.Text;
            String tinhTrang = tinhTrangComboBox.SelectedIndex.ToString();
            String choMuon = choMuonComboBox.SelectedIndex.ToString();
            String maNganTu = maNganTuComboBox.SelectedItem.ToString(); 
          

            List<String> textBoxValues = new List<String>();
            textBoxValues.Add(ISBN);
            textBoxValues.Add(maSach);
            textBoxValues.Add(tinhTrang);
            textBoxValues.Add(choMuon);
            textBoxValues.Add(maNganTu);
           

            List<String> lableForTextBoxValues = new List<String>();
            lableForTextBoxValues.Add("ISBN");
            lableForTextBoxValues.Add("Mã sách");
            lableForTextBoxValues.Add("Tình trạng");
            lableForTextBoxValues.Add("Cho mượn");
            lableForTextBoxValues.Add("Mã ngăn tủ");
           

            List<int> primaryKeyIndex = new List<int>();
            primaryKeyIndex.Add(1);


            myDataGridView = GlobalFunction.saveActionAuto(myDataGridView, isAutoIncreasePrimaryKey, primaryKeyIndex, textBoxValues, lableForTextBoxValues, action, SPInsert, SPUpdate, SPSelect);
            enableActionForSaveAndCancelButton();

        }

        private void myCancelButton_Click(object sender, EventArgs e)
        {
            GlobalFunction.cancelAciton();


            enableActionForSaveAndCancelButton();
        }

        private void mySearchButton_Click(object sender, EventArgs e)
        {
            GlobalFunction.searchData(mySearchTextBox.Text, myDataGridView);
        }

        private void moveFirstButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveFirst();
        }

        private void MoveLastButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveLast();
        }

        private void movePreviousButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MovePrevious();
        }

        private void moveNextButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveNext();
        }


        private void enableActionForSaveAndCancelButton()
        {
            // TODO: change
            // diable textbox
            ISBNComboBox.Enabled = false; 
            maSachTextBox.Enabled = false;
            tinhTrangComboBox.Enabled = false;
            choMuonComboBox.Enabled = false; 
            ISBNComboBox.Enabled = false;
            maNganTuComboBox.Enabled = false;

            ///////////////////////////////////////
            // config button
            myCancelButton.Enabled = false;
            mySaveButton.Enabled = false;
            myAddButton.Enabled = true;
            myEditButton.Enabled = true;
            myDeleteButton.Enabled = true;
            myDataGridView.Enabled = true;
        }
        private void enableActionForAddAndEditButton()
        {
            // TODO: change     
            //config textbox
            if (!isAutoIncreasePrimaryKey)
            {
                maSachTextBox.Enabled = true;
            }
            else
            {
                maSachTextBox.Enabled = false;
            }

            ISBNComboBox.Enabled = true;
            tinhTrangComboBox.Enabled = true;
            choMuonComboBox.Enabled = true; 
            ISBNComboBox.Enabled = true;
            maNganTuComboBox.Enabled = true;

         
            //////////////////////////////////////////
            // config button
            mySaveButton.Enabled = true;
            myCancelButton.Enabled = true;
            myDataGridView.Enabled = false;
            myDeleteButton.Enabled = false;
            myAddButton.Enabled = false;
            myEditButton.Enabled = false;
            maSachTextBox.Focus();

        }

       

        private void maNganTuComboBox_TextChanged(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView)nganTuBindingSource.List[maNganTuComboBox.SelectedIndex];
            moTaTextBox.Text = currentRow["MOTA"].ToString();
        }

        private void ISBNComboBox_TextChanged(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView)ISBNBindingSource.List[ISBNComboBox.SelectedIndex];
            tenSachTextBox.Text = currentRow["TENSACH"].ToString();
        }
    }
}
