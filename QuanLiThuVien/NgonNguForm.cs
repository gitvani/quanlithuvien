﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLiThuVien
{
    public partial class NgonNguForm : Form
    {
        public NgonNguForm()
        {
            InitializeComponent(); 
        }

        private void myForm_Load(object sender, EventArgs e)
        {
            getDataList();
        }
        // TODO: change
        private const String SPSelect = "NGONNGU_XEM" ; 
        private const String SPInsert = "NGONNGU_THEM";
        private const String SPUpdate = "NGONNGU_SUA";
        private const String SPDelete = "NGONNGU_XOA";
        private const Boolean isAutoIncreasePrimaryKey = true; 
        // ---------------------------------
        private void getDataList()
        {
            myDataGridView = GlobalFunction.getDataList(myDataGridView, SPSelect);         
        }
        

        private String action = "";
        private void myAddButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.AddNew();
            action = "Add";
            enableActionForAddAndEditButton();
        }
        private void myEditButton_Click(object sender, EventArgs e)
        {
            action = "Edit";
            enableActionForAddAndEditButton();
        }

        private void myDeleteButton_Click(object sender, EventArgs e)
        {
            // TODO: change
            String primaryKey = maNgonNguTextBox.Text;  
            GlobalFunction.deleteRow(SPDelete, primaryKey, "Mã ngôn ngữ");
            // -----------------------------
            getDataList(); 
          
        }
        private void myDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in myDataGridView.SelectedRows)
            {
                // TODO: change
                maNgonNguTextBox.Text = row.Cells[0].Value.ToString(); 
                tenNgonNguTextBox.Text = row.Cells[1].Value.ToString();
                //------------------------------------------------------------

            }


        }

        private void mySaveButton_Click(object sender, EventArgs e)
        {
            // TODO: change ----------------------------------------------
            String primaryKey = maNgonNguTextBox.Text;  
            String languageName = tenNgonNguTextBox.Text;

            List<String> textBoxValues = new List<String>(); 
            textBoxValues.Add(primaryKey) ;
            textBoxValues.Add( languageName);

            List<String> lableForTextBoxValues = new List<String>();
            lableForTextBoxValues.Add( "Mã ngôn ngữ");
            lableForTextBoxValues.Add("Tên ngôn ngữ");

            Console.WriteLine("otherStringValues " + lableForTextBoxValues[0]); 
            List<int> primaryKeyIndex = new List<int>();
            primaryKeyIndex.Add(0); 

            //String sqlInsert =  "EXEC " + SPInsert + " '" + languageName + "'";
            //String sqlUpdate = "EXEC " + SPUpdate + " '" + primaryKey + "','" + languageName + "'";
            myDataGridView =  GlobalFunction.saveActionAuto(myDataGridView,  isAutoIncreasePrimaryKey,  primaryKeyIndex , textBoxValues, lableForTextBoxValues, action, SPInsert, SPUpdate , SPSelect); 
             //----------------------------------------------     
            enableActionForSaveAndCancelButton();

        }

        private void myCancelButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.CancelEdit();

            enableActionForSaveAndCancelButton();
        }
    
        private void mySearchButton_Click(object sender, EventArgs e)
        { 
            GlobalFunction.searchData(mySearchTextBox.Text ,  myDataGridView); 
        }

        private void moveFirstButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveFirst();
        }

        private void MoveLastButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveLast();
        }

        private void movePreviousButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MovePrevious();
        }

        private void moveNextButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveNext();
        }

   
        private void enableActionForSaveAndCancelButton()
        {
         // diable textbox
            maNgonNguTextBox.Enabled = false;
            tenNgonNguTextBox.Enabled = false;
         // config button
            myCancelButton.Enabled = false;
            mySaveButton.Enabled = false;
            myAddButton.Enabled = true; 
            myEditButton.Enabled = true;
            myDeleteButton.Enabled = true;
            myDataGridView.Enabled = true;
        }
        private void enableActionForAddAndEditButton()
        {
            // config textbox
            if (action =="Add" && !isAutoIncreasePrimaryKey)
            {
                maNgonNguTextBox.Enabled = true;
            }
            else
            {
                maNgonNguTextBox.Enabled = false;
            }
          
            tenNgonNguTextBox.Enabled = true;
            // config button
            mySaveButton.Enabled = true;
            myCancelButton.Enabled = true;
            myDataGridView.Enabled = false;
            myDeleteButton.Enabled = false;
            myAddButton.Enabled = false;
            myEditButton.Enabled = false;
            maNgonNguTextBox.Focus();

        }

       
    }
}
