﻿namespace QuanLiThuVien
{
    partial class TheLoaiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.myDataGridView = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.maTheLoaiTextBox = new System.Windows.Forms.TextBox();
            this.tenTheLoaiTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.themTheLoaibutton = new System.Windows.Forms.Button();
            this.xoaTheLoaiButton = new System.Windows.Forms.Button();
            this.SuaTheLoaiButton = new System.Windows.Forms.Button();
            this.searchTheLoaiTextBox = new System.Windows.Forms.TextBox();
            this.searchTheLoaiButton = new System.Windows.Forms.Button();
            this.moveFirstButton = new System.Windows.Forms.Button();
            this.MoveLastButton = new System.Windows.Forms.Button();
            this.movePreviousButton = new System.Windows.Forms.Button();
            this.moveNextButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ghiTheLoaiButton = new System.Windows.Forms.Button();
            this.BoQuaTheLoaiButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.myDataGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // myDataGridView
            // 
            this.myDataGridView.AllowUserToAddRows = false;
            this.myDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.myDataGridView.Location = new System.Drawing.Point(83, 40);
            this.myDataGridView.Name = "myDataGridView";
            this.myDataGridView.ReadOnly = true;
            this.myDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.myDataGridView.Size = new System.Drawing.Size(624, 150);
            this.myDataGridView.TabIndex = 0;
            this.myDataGridView.SelectionChanged += new System.EventHandler(this.theLoaiDataGridView_SelectionChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(80, 262);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mã thể loại: ";
            // 
            // maTheLoaiTextBox
            // 
            this.maTheLoaiTextBox.Enabled = false;
            this.maTheLoaiTextBox.Location = new System.Drawing.Point(151, 259);
            this.maTheLoaiTextBox.Name = "maTheLoaiTextBox";
            this.maTheLoaiTextBox.Size = new System.Drawing.Size(149, 20);
            this.maTheLoaiTextBox.TabIndex = 2;
            // 
            // tenTheLoaiTextBox
            // 
            this.tenTheLoaiTextBox.Enabled = false;
            this.tenTheLoaiTextBox.Location = new System.Drawing.Point(151, 298);
            this.tenTheLoaiTextBox.Name = "tenTheLoaiTextBox";
            this.tenTheLoaiTextBox.Size = new System.Drawing.Size(149, 20);
            this.tenTheLoaiTextBox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(80, 301);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Tên thể loại: ";
            // 
            // themTheLoaibutton
            // 
            this.themTheLoaibutton.Location = new System.Drawing.Point(6, 19);
            this.themTheLoaibutton.Name = "themTheLoaibutton";
            this.themTheLoaibutton.Size = new System.Drawing.Size(100, 23);
            this.themTheLoaibutton.TabIndex = 6;
            this.themTheLoaibutton.Text = "Thêm";
            this.themTheLoaibutton.UseVisualStyleBackColor = true;
            this.themTheLoaibutton.Click += new System.EventHandler(this.themTheLoaibutton_Click);
            // 
            // xoaTheLoaiButton
            // 
            this.xoaTheLoaiButton.Location = new System.Drawing.Point(230, 19);
            this.xoaTheLoaiButton.Name = "xoaTheLoaiButton";
            this.xoaTheLoaiButton.Size = new System.Drawing.Size(100, 23);
            this.xoaTheLoaiButton.TabIndex = 7;
            this.xoaTheLoaiButton.Text = "Xóa";
            this.xoaTheLoaiButton.UseVisualStyleBackColor = true;
            this.xoaTheLoaiButton.Click += new System.EventHandler(this.xoaTheLoaiButton_Click);
            // 
            // SuaTheLoaiButton
            // 
            this.SuaTheLoaiButton.Location = new System.Drawing.Point(118, 19);
            this.SuaTheLoaiButton.Name = "SuaTheLoaiButton";
            this.SuaTheLoaiButton.Size = new System.Drawing.Size(100, 23);
            this.SuaTheLoaiButton.TabIndex = 8;
            this.SuaTheLoaiButton.Text = "Sửa";
            this.SuaTheLoaiButton.UseVisualStyleBackColor = true;
            this.SuaTheLoaiButton.Click += new System.EventHandler(this.SuaTheLoaiButton_Click);
            // 
            // searchTheLoaiTextBox
            // 
            this.searchTheLoaiTextBox.Location = new System.Drawing.Point(499, 14);
            this.searchTheLoaiTextBox.Name = "searchTheLoaiTextBox";
            this.searchTheLoaiTextBox.Size = new System.Drawing.Size(127, 20);
            this.searchTheLoaiTextBox.TabIndex = 9;
            // 
            // searchTheLoaiButton
            // 
            this.searchTheLoaiButton.Location = new System.Drawing.Point(632, 11);
            this.searchTheLoaiButton.Name = "searchTheLoaiButton";
            this.searchTheLoaiButton.Size = new System.Drawing.Size(75, 23);
            this.searchTheLoaiButton.TabIndex = 10;
            this.searchTheLoaiButton.Text = "Tìm kiếm";
            this.searchTheLoaiButton.UseVisualStyleBackColor = true;
            this.searchTheLoaiButton.Click += new System.EventHandler(this.searchTheLoaiButton_Click);
            // 
            // moveFirstButton
            // 
            this.moveFirstButton.Location = new System.Drawing.Point(6, 11);
            this.moveFirstButton.Name = "moveFirstButton";
            this.moveFirstButton.Size = new System.Drawing.Size(30, 23);
            this.moveFirstButton.TabIndex = 11;
            this.moveFirstButton.Text = "<<";
            this.moveFirstButton.UseVisualStyleBackColor = true;
            this.moveFirstButton.Click += new System.EventHandler(this.moveFirstButton_Click);
            // 
            // MoveLastButton
            // 
            this.MoveLastButton.Location = new System.Drawing.Point(114, 11);
            this.MoveLastButton.Name = "MoveLastButton";
            this.MoveLastButton.Size = new System.Drawing.Size(30, 23);
            this.MoveLastButton.TabIndex = 12;
            this.MoveLastButton.Text = ">>";
            this.MoveLastButton.UseVisualStyleBackColor = true;
            this.MoveLastButton.Click += new System.EventHandler(this.MoveLastButton_Click);
            // 
            // movePreviousButton
            // 
            this.movePreviousButton.Location = new System.Drawing.Point(42, 11);
            this.movePreviousButton.Name = "movePreviousButton";
            this.movePreviousButton.Size = new System.Drawing.Size(30, 23);
            this.movePreviousButton.TabIndex = 13;
            this.movePreviousButton.Text = "<";
            this.movePreviousButton.UseVisualStyleBackColor = true;
            this.movePreviousButton.Click += new System.EventHandler(this.movePreviousButton_Click);
            // 
            // moveNextButton
            // 
            this.moveNextButton.Location = new System.Drawing.Point(78, 11);
            this.moveNextButton.Name = "moveNextButton";
            this.moveNextButton.Size = new System.Drawing.Size(30, 23);
            this.moveNextButton.TabIndex = 14;
            this.moveNextButton.Text = ">";
            this.moveNextButton.UseVisualStyleBackColor = true;
            this.moveNextButton.Click += new System.EventHandler(this.moveNextButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.moveFirstButton);
            this.groupBox1.Controls.Add(this.moveNextButton);
            this.groupBox1.Controls.Add(this.MoveLastButton);
            this.groupBox1.Controls.Add(this.movePreviousButton);
            this.groupBox1.Location = new System.Drawing.Point(83, 196);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(149, 44);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            // 
            // ghiTheLoaiButton
            // 
            this.ghiTheLoaiButton.Enabled = false;
            this.ghiTheLoaiButton.Location = new System.Drawing.Point(56, 50);
            this.ghiTheLoaiButton.Name = "ghiTheLoaiButton";
            this.ghiTheLoaiButton.Size = new System.Drawing.Size(100, 23);
            this.ghiTheLoaiButton.TabIndex = 16;
            this.ghiTheLoaiButton.Text = "Ghi";
            this.ghiTheLoaiButton.UseVisualStyleBackColor = true;
            this.ghiTheLoaiButton.Click += new System.EventHandler(this.ghiTheLoaiButton_Click);
            // 
            // BoQuaTheLoaiButton
            // 
            this.BoQuaTheLoaiButton.Enabled = false;
            this.BoQuaTheLoaiButton.Location = new System.Drawing.Point(162, 49);
            this.BoQuaTheLoaiButton.Name = "BoQuaTheLoaiButton";
            this.BoQuaTheLoaiButton.Size = new System.Drawing.Size(100, 23);
            this.BoQuaTheLoaiButton.TabIndex = 17;
            this.BoQuaTheLoaiButton.Text = "Bỏ qua";
            this.BoQuaTheLoaiButton.UseVisualStyleBackColor = true;
            this.BoQuaTheLoaiButton.Click += new System.EventHandler(this.phucHoiTheLoaiButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.themTheLoaibutton);
            this.groupBox2.Controls.Add(this.BoQuaTheLoaiButton);
            this.groupBox2.Controls.Add(this.ghiTheLoaiButton);
            this.groupBox2.Controls.Add(this.SuaTheLoaiButton);
            this.groupBox2.Controls.Add(this.xoaTheLoaiButton);
            this.groupBox2.Location = new System.Drawing.Point(367, 207);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(340, 83);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            // 
            // TheLoaiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(791, 366);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.searchTheLoaiButton);
            this.Controls.Add(this.searchTheLoaiTextBox);
            this.Controls.Add(this.tenTheLoaiTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.maTheLoaiTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.myDataGridView);
            this.Name = "TheLoaiForm";
            this.Text = "TheLoaiForm";
            this.Load += new System.EventHandler(this.TheLoaiForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.myDataGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView myDataGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox maTheLoaiTextBox;
        private System.Windows.Forms.TextBox tenTheLoaiTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button themTheLoaibutton;
        private System.Windows.Forms.Button xoaTheLoaiButton;
        private System.Windows.Forms.Button SuaTheLoaiButton;
        private System.Windows.Forms.TextBox searchTheLoaiTextBox;
        private System.Windows.Forms.Button searchTheLoaiButton;
        private System.Windows.Forms.Button moveFirstButton;
        private System.Windows.Forms.Button MoveLastButton;
        private System.Windows.Forms.Button movePreviousButton;
        private System.Windows.Forms.Button moveNextButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button ghiTheLoaiButton;
        private System.Windows.Forms.Button BoQuaTheLoaiButton;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}