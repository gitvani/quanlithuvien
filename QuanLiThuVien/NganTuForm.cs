﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLiThuVien
{
    public partial class NganTuForm : Form
    {
        public NganTuForm()
        {
            InitializeComponent();
        }
        private void myForm_Load(object sender, EventArgs e)
        {
            getDataList();
        }
        // TODO: change
        private const String SPSelect = "NGANTU_XEM";
        private const String SPInsert = "NGANTU_THEM";
        private const String SPUpdate = "NGANTU_SUA";
        private const String SPDelete = "NGANTU_XOA";
        private const Boolean isAutoIncreasePrimaryKey = true;
        // ---------------------------------
        private void getDataList()
        {
            myDataGridView = GlobalFunction.getDataList(myDataGridView, SPSelect);
        }




        private String action = "";
        private void myAddButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.AddNew();
            action = "Add";
            enableActionForAddAndEditButton();
        }
        private void myEditButton_Click(object sender, EventArgs e)
        {
            action = "Edit";
            enableActionForAddAndEditButton();
        }

        private void myDeleteButton_Click(object sender, EventArgs e)
        {
            // TODO: change
            String primaryKey = maNganTuTextBox.Text;
            GlobalFunction.deleteRow(SPDelete, primaryKey, "Mã tác giả");
            // -----------------------------
            getDataList();

        }
        private void myDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in myDataGridView.SelectedRows)
            {
                // TODO: change
                maNganTuTextBox.Text = row.Cells[0].Value.ToString();
                moTaTextBox.Text = row.Cells[1].Value.ToString();
            




                //------------------------------------------------------------

            }


        }

        private void mySaveButton_Click(object sender, EventArgs e)
        {
            // TODO: change ----------------------------------------------
            String maNganTu = maNganTuTextBox.Text;
            String moTa = moTaTextBox.Text;
     


            List<String> textBoxValues = new List<String>();
            textBoxValues.Add(maNganTu);
            textBoxValues.Add(moTa);
         

            List<String> lableForTextBoxValues = new List<String>();
            lableForTextBoxValues.Add("Mã ngăn tủ");
            lableForTextBoxValues.Add("Mô tả");
          

            List<int> primaryKeyIndex = new List<int>();
            primaryKeyIndex.Add(0);


            myDataGridView = GlobalFunction.saveActionAuto(myDataGridView, isAutoIncreasePrimaryKey, primaryKeyIndex, textBoxValues, lableForTextBoxValues, action, SPInsert, SPUpdate, SPSelect);
            //----------------------------------------------     
            enableActionForSaveAndCancelButton();

        }

        private void myCancelButton_Click(object sender, EventArgs e)
        {
            GlobalFunction.cancelAciton();
            enableActionForSaveAndCancelButton();
        }

        private void mySearchButton_Click(object sender, EventArgs e)
        {
            GlobalFunction.searchData(mySearchTextBox.Text, myDataGridView);
        }

        private void moveFirstButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveFirst();
        }

        private void MoveLastButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveLast();
        }

        private void movePreviousButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MovePrevious();
        }

        private void moveNextButton_Click(object sender, EventArgs e)
        {
            Program.bindingSource.MoveNext();
        }


        private void enableActionForSaveAndCancelButton()
        {
            // TODO: change
            // diable textbox
            maNganTuTextBox.Enabled = false;
            moTaTextBox.Enabled = false;


            ///////////////////////////////////////
            // config button
            myCancelButton.Enabled = false;
            mySaveButton.Enabled = false;
            myAddButton.Enabled = true;
            myEditButton.Enabled = true;
            myDeleteButton.Enabled = true;
            myDataGridView.Enabled = true;
        }
        private void enableActionForAddAndEditButton()
        {
            // TODO: change     
            //config textbox
            if (action == "Add" && !isAutoIncreasePrimaryKey)
            {
                maNganTuTextBox.Enabled = true;
            }
            else
            {
                maNganTuTextBox.Enabled = false;
            }

            moTaTextBox.Enabled = true;
         

            //////////////////////////////////////////
            // config button
            mySaveButton.Enabled = true;
            myCancelButton.Enabled = true;
            myDataGridView.Enabled = false;
            myDeleteButton.Enabled = false;
            myAddButton.Enabled = false;
            myEditButton.Enabled = false;
            maNganTuTextBox.Focus();

        }
    }
}
