﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLiThuVien
{
    class GlobalFunction
    {
        public static String oldPrimaryKey;
        public static String convertBitToString(String bit, String value0, String value1)
        {
            return (bit.Equals("False")) ? value0 : value1;
        }
     
        public static String convertStringToBit(String stringData, String value0, String value1)
        {
            return (stringData.Equals(value0)) ?  "0" : "1";
        }
        public static DataGridView getDataList(DataGridView myDataGridView, String SPSelect ) {
             myDataGridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);
            Program.GetData("exec "+SPSelect);
            myDataGridView.DataSource = Program.bindingSource;
            // resize column 
            int columnWidth = (myDataGridView.Width - myDataGridView.RowHeadersWidth) / myDataGridView.ColumnCount;
            for (int i = 0; i < myDataGridView.ColumnCount; i++)
            {
                myDataGridView.Columns[i].Width = columnWidth;
            }
            return myDataGridView; 
        }

        public static void deleteRow(  String SPDelete  , String primaryKey , String primaryKeyLabel)
        {
            if (primaryKey.Trim().Length == 0)
            {
                MessageBox.Show(primaryKeyLabel + " không thể bỏ trống.", "Lỗi", MessageBoxButtons.OK);
               
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Bạn có thật sự muốn xóa đối tượng có mã là " + primaryKey, "Cảnh báo", MessageBoxButtons.OKCancel);
                if (dialogResult == DialogResult.OK)
                {
                    String sql = "EXEC " + SPDelete + " '" + primaryKey + "'";
                    Program.editData(sql);
                   
                }
            }
            
            
        }
        public static DataGridView saveAction( DataGridView myDataGridView ,  Boolean isAutoIncreasePrimaryKey, List<int>primaryKeyIndex ,   List<String> textBoxValues  , List<String> lableForTextBoxValues , String action , String   sqlInsert, String sqlUpdate , String SPSelect)
        {    
            try { 

                 for(int i = 0 ; i < textBoxValues.Count ; i++) {                    
                         for (int j = 0; j < primaryKeyIndex.Count; j++)
                         {
                             if (i == primaryKeyIndex[j]  )
                             {
                                 if ( !isAutoIncreasePrimaryKey && textBoxValues[i].Trim().Length == 0)
                                 {
                                     MessageBox.Show("Khóa " + lableForTextBoxValues[i] + " không thể bỏ trống.", "Lỗi", MessageBoxButtons.OK);
                                     return myDataGridView;
                                 }
                             } else {
                                 if (textBoxValues[i].Trim().Length == 0)
                                 {
                                     MessageBox.Show(lableForTextBoxValues[i] + " không thể bỏ trống.", "Lỗi", MessageBoxButtons.OK);
                                     return myDataGridView;

                                 }
                             }
                         }              
               }




                 String sql = "";
                if (action.Equals("Add"))
                {
                    sql = sqlInsert; 

                }
                else if (action.Equals("Edit"))
                {
                    sql = sqlUpdate; 
                }

                Program.editData(sql);

                // move to the currently updated or edited row
                if (isAutoIncreasePrimaryKey)
                {
                    int index = myDataGridView.CurrentRow.Index;
                    myDataGridView = getDataList(myDataGridView, SPSelect);
                    myDataGridView.CurrentCell = myDataGridView.Rows[index].Cells[0];
                }
                else
                {
                    myDataGridView = getDataList(myDataGridView, SPSelect);
                    myDataGridView =  searchData(textBoxValues[primaryKeyIndex[0]], myDataGridView); 
                }
               
                return myDataGridView;


            }  
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Number.ToString() ,"Lỗi" , MessageBoxButtons.OK);
                Console.WriteLine(ex.Number.ToString()); 
                if (ex.Number == 2627)       //Violation of primary key. Handle Exception
                {
                    String message = "Khóa chính " + textBoxValues[primaryKeyIndex[0]] + " đã bị trùng lắp.\n" ; 
                    MessageBox.Show(message, "Lỗi", MessageBoxButtons.OK);
                   // Program.bindingSource.CancelEdit();
                  //  myDataGridView = searchData(textBoxValues[primaryKeyIndex[0]], myDataGridView); 

                }
              
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi : " + ex.Message, "Lỗi", MessageBoxButtons.OK);
            }

            return myDataGridView;
        
        }
        public static DataGridView searchData(String keyword, DataGridView myDataGridView)
               
        {
            string searchValue = keyword.Trim() ; 
            List<DataGridViewRow> searchMatchRow = new List<DataGridViewRow>();
            myDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            if (searchValue != null && searchValue.Length > 0)
            {
                try
                {

                    foreach (DataGridViewRow row in myDataGridView.Rows)
                    {
                        // row.Selected = false;
                        for (int i = 0; i < myDataGridView.ColumnCount; i++)
                        {

                            if (row.Cells[i].Value.ToString().Trim().Equals(searchValue))
                            {
                                row.Selected = true;
                                searchMatchRow.Add(row);
                              
                            }


                        }
                    }
                    if (searchMatchRow.Count > 0)
                    {
                        myDataGridView.CurrentCell = searchMatchRow[0].Cells[0];
                    }
                    else
                    {
                        MessageBox.Show("Không tìm thấy thông tin liên quan đến " + searchValue);
                    }

                }

                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }

             
            }
            return myDataGridView; 
        }









        public static DataGridView saveActionAuto(DataGridView myDataGridView, Boolean isAutoIncreasePrimaryKey, List<int> primaryKeyIndex, List<String> textBoxValues, List<String> lableForTextBoxValues, String action, String SPInsert, String SPUpdate, String SPSelect)
        {
            try
            {

                for (int i = 0; i < textBoxValues.Count; i++)
                {
                    for (int j = 0; j < primaryKeyIndex.Count; j++)
                    {
                        if (i == primaryKeyIndex[j])
                        {
                            if (!isAutoIncreasePrimaryKey && textBoxValues[i].Trim().Length == 0)
                            {
                                MessageBox.Show("Khóa " + lableForTextBoxValues[i] + " không thể bỏ trống.", "Lỗi", MessageBoxButtons.OK);
                                return myDataGridView;
                            }
                        }
                        else
                        {
                            if (textBoxValues[i].Trim().Length == 0)
                            {
                                MessageBox.Show(lableForTextBoxValues[i] + " không thể bỏ trống.", "Lỗi", MessageBoxButtons.OK);
                                return myDataGridView;

                            }
                        }
                    }




                }


                String sqlInsert = "EXEC " + SPInsert;
                int start = (isAutoIncreasePrimaryKey == true) ? 1 : 0; //not 0 because don't use primarykey
                for (int i = start; i < textBoxValues.Count; i++)
                {
                    sqlInsert += " '";
                    sqlInsert += textBoxValues[i];
                    sqlInsert += "'";
                    if (i != textBoxValues.Count - 1)
                    {
                        sqlInsert += ",";
                    }
                }

                Console.WriteLine(sqlInsert);

                String sqlUpdate = "EXEC " + SPUpdate;
                if (!isAutoIncreasePrimaryKey)
                {
                    sqlUpdate += " '"+oldPrimaryKey+"',"; 
                }
                for (int i = 0; i < textBoxValues.Count; i++)
                {
                    sqlUpdate += " '";
                    sqlUpdate += textBoxValues[i];
                    sqlUpdate += "'";
                    if (i != textBoxValues.Count - 1)
                    {
                        sqlUpdate += ",";
                    }
                }

                String sql = "";
                if (action.Equals("Add"))
                {
                    sql = sqlInsert;

                }
                else if (action.Equals("Edit"))
                {
                    sql = sqlUpdate;
                }

                Program.editData(sql);

                // move to the currently updated or edited row
                if (isAutoIncreasePrimaryKey)
                {
                    int index = myDataGridView.CurrentRow.Index;
                    myDataGridView = getDataList(myDataGridView, SPSelect);
                    myDataGridView.CurrentCell = myDataGridView.Rows[index].Cells[0];
                }
                else
                {
                    myDataGridView = getDataList(myDataGridView, SPSelect);
                    myDataGridView = searchData(textBoxValues[primaryKeyIndex[0]], myDataGridView);
                }

                return myDataGridView;


            }
            catch (SqlException ex)
            {
               
               
                if (ex.Number == 2627)       //Violation of primary key. Handle Exception
                {
                    String message = "Khóa chính " + textBoxValues[primaryKeyIndex[0]] + " đã bị trùng lắp.\n";
                    MessageBox.Show(message, "Lỗi " +ex.Number, MessageBoxButtons.OK);
                    Program.bindingSource.CancelEdit();
                    myDataGridView = searchData(textBoxValues[primaryKeyIndex[0]], myDataGridView);
                }
                else if (ex.Number == 8114)
                {
                    String message = "Kiễu dữ liệu không đúng: " +ex.Message  ;
                    MessageBox.Show(message, "Lỗi " +ex.Number, MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show(ex.Message, "Lỗi " + ex.Number.ToString(), MessageBoxButtons.OK);
               
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi : " + ex.Message, "Lỗi", MessageBoxButtons.OK);
            }

            return myDataGridView;

        }
        public static void cancelAciton()
        {
            Program.bindingSource.CancelEdit();
            //Program.bindingSource.MovePrevious();
            //Program.bindingSource.MoveNext();

           
         
        }
    }
}
