﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLiThuVien
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void DangNhap_Load(object sender, EventArgs e)
        {

            //LOGIN
            try
            {
                RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server");
                String[] instances = (String[])rk.GetValue("InstalledInstances");
                if (instances.Length > 0)
                {
                    foreach (String element in instances)
                    {
                        if (element == "MSSQLSERVER")
                        {
                            comboBoxServer.Items.Add(System.Environment.MachineName);
                        }
                        else
                        {
                            comboBoxServer.Items.Add(System.Environment.MachineName + @"\" + element);
                        }

                    }
                }
                comboBoxServer.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Không load được server. ERROR: " + ex.Message,"Thông báo", MessageBoxButtons.OK); 
            
            }
          
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            Program.mlogin = loginNameTextBox.Text;
            Program.password = textBoxPassword.Text;
            Program.serverName = comboBoxServer.SelectedItem.ToString();
           
            if (Program.KetNoi() == 0) return;

           // String strQuery = "SELECT name FROM sys.sysusers WHERE sid = SUSER_SID('" + Program.mlogin+"')";
            String strQuery = "exec LAYTHONGTIN '" + Program.mlogin + "'";  
            Program.myReader = Program.ExecSqlDataReader(strQuery, Program.connectionString);
            Program.myReader.Read(); 
            Program.username = Program.myReader.GetString(0)  ; 
            Program.fullName = Program.myReader.GetString(1);
            Program.role = Program.myReader.GetString(2); 
                if (Convert.IsDBNull(Program.username))
                {
                    MessageBox.Show("Login bạn nhập không có quyền truy cập dữ liệu", "Thông báo", MessageBoxButtons.OK);
                    return; 
                }
                Program.myReader.Close();

                MainMenu MainMenu = new MainMenu();
                this.TopLevel = false;
               MainMenu.Show();
               
               
              //  strQuery = "sp_helpuser '" + Program.username + "'";  
            
        }
    }
}
